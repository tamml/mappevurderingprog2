package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The Matrix2x2NegativeTest class contains negative test cases for the Matrix2x2 class.
 */
public class Matrix2x2PositiveTest {

  /**
   * Tests the matrix2x2Constructor method of Matrix2x2 with positive input.
   * Verifies that the matrix is initialized with all 1's.
   */
  @Test
  public void matrix2x2ConstructorPositiveTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    System.out.println(matrix1.getA00() + " " + matrix1.getA01() + " " + matrix1.getA10() + " " + matrix1.getA11());
  }

  /**
   * Tests the matrix2x2GetA00 method of Matrix2x2 with positive input.
   * Verifies that the value of a00 is 1.
   */
  @Test
  public void matrix2x2GetA00PositiveTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    System.out.println(matrix1.getA00());
  }

  /**
   * Tests the matrix2x2GetA01 method of Matrix2x2 with positive input.
   * Verifies that the value of a01 is 1.
   */
  @Test
  public void matrix2x2GetA01PositiveTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    System.out.println(matrix1.getA01());
  }

  /**
   * Tests the matrix2x2GetA10 method of Matrix2x2 with positive input.
   * Verifies that the value of a10 is 1.
   */
  @Test
  public void matrix2x2GetA10PositiveTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    System.out.println(matrix1.getA10());
  }

  /**
   * Tests the matrix2x2GetA11 method of Matrix2x2 with positive input.
   * Verifies that the value of a11 is 1.
   */
  @Test
  public void matrix2x2GetA11PositiveTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    System.out.println(matrix1.getA11());
  }

  /**
   * Tests the matrix2x2Multiply method of Matrix2x2 with positive input.
   * Verifies that the result is not null and an instance of Vector2D.
   */
  @Test
  public void matrix2x2MultiplyPositiveTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    Vector2D vector1 = new Vector2D(1, 1);
    Vector2D result = matrix1.multiply(vector1);
    System.out.println(result.getX0() + " " + result.getX1());
  }
}