package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameFileHandler;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameFileHandlerNegativeTest class contains negative test cases
 * for the ChaosGameFileHandler class.
 */
public class ChaosGameFileHandlerNegativeTest {

  /**
   * Tests the readTransformationsFromNFile method with negative input.
   */
  @Test
  public void readTransformationsFromNFileNegativeTest() {
    String fileName = "nonExistentFile.txt";
    assertThrows(FileNotFoundException.class, () -> {
      ChaosGameFileHandler.readTransformationsFromFile(fileName);
    });
  }

  /**
   * Tests the writeTransformationsToInvalidPath method with negative input.
   */
  @Test
  public void writeTransformationsToInvalidPathTest() {
    String invalidPath = "/invalid/path/testTransformations.txt";
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(1.0, 1.0)));

    assertThrows(Exception.class, () -> {
      ChaosGameFileHandler.writeTransformationsToFile(new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(1, 1)), invalidPath);
    });
  }
}
