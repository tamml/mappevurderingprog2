package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The Vector2DNegativeTest class contains negative test cases for the Vector2D class.
 */
public class Vector2DNegativeTest {

  /**
   * Tests the vector2DConstructor method of Vector2D with negative input.
   * Verifies that the vector is not initialized with all 1's.
   */
  @Test
  public void vector2DConstructorNegativeTest() {
    Vector2D vector1 = new Vector2D(0, 0);
    assert vector1.getX0() != 1;
    assert vector1.getX1() != 1;
  }

  /**
   * Tests the getX0 method of Vector2D with negative input.
   * Verifies that the value of x0 is not 0.
   */
  @Test
  public void getX0NegativeTest() {
    Vector2D vector1 = new Vector2D(1, 1);
    assert vector1.getX0() != 0;
  }

  /**
   * Tests the getX1 method of Vector2D with negative input.
   * Verifies that the value of x1 is not 0.
   */
  @Test
  public void getX1NegativeTest() {
    Vector2D vector1 = new Vector2D(1, 1);
    assert vector1.getX1() != 0;
  }

  /**
   * Tests the add method of Vector2D with negative input.
   * Verifies that the x0 and x1 values of the result are not 2.
   */
  @Test
  public void addNegativeTest() {
    try {
      Vector2D vector1 = new Vector2D(2, 2);
      Vector2D result = vector1.add(null);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Tests the subtract method of Vector2D with negative input.
   * Verifies that the x0 and x1 values of the result are not 2.
   */
  @Test
  public void subtractNegativeTest() {
    try {
      Vector2D vector1 = new Vector2D(3, 1);
      Vector2D result = vector1.subtract(null);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }
}