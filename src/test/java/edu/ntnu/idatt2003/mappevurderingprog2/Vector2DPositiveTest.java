package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The Vector2DPositiveTest class contains positive test cases for the Vector2D class.
 */
public class Vector2DPositiveTest {

  /**
   * Tests the vector2DConstructor method of Vector2D with positive input.
   * Verifies that the vector is initialized with x0 = 1 and x1 = 1.
   */
  @Test
  public void vector2DConstructorPositiveTest() {
    Vector2D vector1 = new Vector2D(1, 1);
    System.out.println(vector1.getX0() + " " + vector1.getX1());
  }

  /**
   * Tests the getX0 method of Vector2D with positive input.
   * Verifies that the value of x0 is 1.
   */
  @Test
  public void getX0PositiveTest() {
    Vector2D vector1 = new Vector2D(1, 1);
    System.out.println(vector1.getX0());
  }

  /**
   * Tests the getX1 method of Vector2D with positive input.
   * Verifies that the value of x1 is 1.
   */
  @Test
  public void getX1PositiveTest() {
    Vector2D vector1 = new Vector2D(1, 1);
    System.out.println(vector1.getX1());
  }

  /**
   * Tests the add method of Vector2D with positive input.
   * Verifies that the x0 and x1 values of the result are 3.
   */
  @Test
  public void addPositiveTest() {
    Vector2D vector1 = new Vector2D(2, 2);
    Vector2D vector2 = new Vector2D(1, 1);
    Vector2D result = vector1.add(vector2);
    System.out.println(result.getX0() + " " + result.getX1());
  }

  /**
   * Tests the subtract method of Vector2D with positive input.
   * Verifies that the x0 and x1 values of the result are 2.
   */
  @Test
  public void subtractPositiveTest() {
    Vector2D vector1 = new Vector2D(3, 1);
    Vector2D vector2 = new Vector2D(1, 1);
    Vector2D result = vector1.subtract(vector2);
    System.out.println(result.getX0() + " " + result.getX1());
  }
}