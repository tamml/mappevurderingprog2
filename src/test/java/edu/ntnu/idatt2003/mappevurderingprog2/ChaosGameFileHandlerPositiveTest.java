package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameFileHandler;
import java.io.File;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameFileHandlerPositiveTest class contains positive test cases for the ChaosGameFileHandler class.
 */
public class ChaosGameFileHandlerPositiveTest {

  /**
   * Tests the writeTransformationsToFile method with positive input.
   *
   * @throws Exception if an error occurs during the test.
   */
  @Test
  public void writeTransformationsFromFilePositiveTest() throws Exception {
    String fileName = "testTransformations.txt";
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(1.0, 1.0))
    );
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCoords, maxCoords);

    ChaosGameFileHandler.writeTransformationsToFile(description, fileName);
    ChaosGameDescription actualDescription = ChaosGameFileHandler.readTransformationsFromFile(fileName);
    assertEquals(description.getTransforms().size(), actualDescription.getTransforms().size());
    assert (description.getTransforms().size() == actualDescription.getTransforms().size());
    assert (description.getMinCoords().getX0() == actualDescription.getMinCoords().getX0());
    assert (description.getMinCoords().getX1() == actualDescription.getMinCoords().getX1());
    assert (description.getMaxCoords().getX0() == actualDescription.getMaxCoords().getX0());
  }

  /**
   * Tests the readTransformationsFromFile method with positive input.
   *
   * @throws Exception if an error occurs during the test.
   */
  @Test
  public void readTransformationsFromFilePositiveTest() throws Exception {
    String fileName = "testTransformations.txt";
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(1.0, 1.0))
    );
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    ChaosGameDescription expectedDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGameFileHandler.writeTransformationsToFile(expectedDescription, fileName);
    ChaosGameDescription actualDescription = ChaosGameFileHandler.readTransformationsFromFile(fileName);
    assert (expectedDescription.getTransforms().size() == actualDescription.getTransforms().size());
    assert (expectedDescription.getMinCoords().getX0() == actualDescription.getMinCoords().getX0());
    assert (expectedDescription.getMinCoords().getX1() == actualDescription.getMinCoords().getX1());
    assert (expectedDescription.getMaxCoords().getX0() == actualDescription.getMaxCoords().getX0());
    assert (expectedDescription.getMaxCoords().getX1() == actualDescription.getMaxCoords().getX1());
  }

  /**
   * Tests the listTransformationFileNames method with positive input.
   *
   * @throws Exception if an error occurs during the test.
   */
  @Test
  public void listTransformationFileNamesPositiveTest() throws Exception {
    String fileName = "testTransformations.txt";
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(1.0, 1.0))
    );
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGameFileHandler.writeTransformationsToFile(description, fileName);
    List<String> fileNames = ChaosGameFileHandler.listTransformationFileNames();
    assertTrue(fileNames.contains(fileName));
  }

  /**
   * Tests the checkFileExists method with positive input.
   *
   * @throws Exception if an error occurs during the test.
   */
  @Test
  public void checkFileExistsPositiveTest() throws Exception {
    String fileName = "testTransformations.txt";
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(1.0, 1.0))
    );
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGameFileHandler.writeTransformationsToFile(description, fileName);
    assertTrue(ChaosGameFileHandler.checkFileExists(fileName));
  }

  /**
   * Cleans up any resources or files created during the test.
   * Deletes the testTransformations.txt file created during the test execution.
   */
  @AfterEach
  public void cleanup() {
    new File("src/main/resources/edu/ntnu/idatt2003/mappevurderingprog2/transformations/testTransformations.txt").delete(); // Clean up file after test
  }
}
