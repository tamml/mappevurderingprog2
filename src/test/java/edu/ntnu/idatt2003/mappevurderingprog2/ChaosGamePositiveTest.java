package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.CanvasController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGame;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.views.View;

import java.awt.Canvas;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGamePositiveTest class contains positive test cases for the ChaosGame class.
 */
public class ChaosGamePositiveTest {

  /**
   * Resets the ChaosGame instance before each test.
   */
  @BeforeEach
  public void tearDown() {
    ChaosGame.getInstance().setDescription(null);
    ChaosGame.getInstance().resetOutOfBoundsCount();
    ChaosGame.getInstance().getObservers().clear();
    ChaosGame.getInstance().setSteps(0);
  }

  /**
   * Tests the getInstance method with valid input.
   * Verifies that the ChaosGame instance is not null.
   */
  @Test
  public void chaosGameGetInstancePositiveTest() {
    ChaosGame chaosGame = ChaosGame.getInstance();
    assertNotNull(chaosGame);
  }

  /**
   * Tests setting a description that is allowed.
   * Verifies that the description is the same
   * as the one that was attempted to be set.
   */
  @Test
  public void chaosGameSetDescriptionPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGame.getInstance().setDescription(chaosGameDescription);
    assertEquals(chaosGameDescription, ChaosGame.getInstance().getDescription());
  }

  /**
   * Tests setting a canvas that is allowed.
   * Verifies that the canvas is not null.
   */
  @Test
  public void setCanvasPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGame.getInstance().setDescription(chaosGameDescription);
    ChaosGame.getInstance().setCanvas();
    assertNotNull(ChaosGame.getInstance().getCanvas());
  }

  /**
   * Tests setting steps that are allowed.
   * Verifies that the steps are the same
   * as the ones that were attempted to be set.
   */
  @Test
  public void setStepsPositiveTest() {
    ChaosGame.getInstance().setSteps(100);
    assertEquals(100, ChaosGame.getInstance().getSteps());
  }

  /**
   * Tests adding an observer that is allowed.
   * Verifies that the observer list size is 1.
   */
  @Test
  public void addObserverPositiveTest() {
    GameController gameController = new GameController();
    CanvasController canvasController = new CanvasController();
    FileController fileController = new FileController();
    View view = new View(gameController, canvasController, fileController);
    ChaosGame.getInstance().addObserver(view);
    assertEquals(1, ChaosGame.getInstance().getObservers().size());
  }

  /**
   * Tests removing an observer that is allowed.
   * Verifies that the observer list size is 0.
   */
  @Test
  public void removeObserverPositiveTest() {
    GameController gameController = new GameController();
    CanvasController canvasController = new CanvasController();
    FileController fileController = new FileController();
    View view = new View(gameController, canvasController, fileController);
    ChaosGame.getInstance().addObserver(view);
    ChaosGame.getInstance().removeObserver(view);
    assertEquals(0, ChaosGame.getInstance().getObservers().size());
  }

  /**
   * Tests resetting the out of bounds count that is allowed.
   * Verifies that the out of bounds count is 0.
   */
  @Test
  public void getCanvasPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGame.getInstance().setDescription(chaosGameDescription);
    ChaosGame.getInstance().setCanvas();
    assertNotNull(ChaosGame.getInstance().getCanvas());
  }

  /**
   * Tests getting a description that is allowed.
   * Verifies that the description is the same
   * as the one that was attempted to be set.
   */
  @Test
  public void getDescriptionPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGame.getInstance().setDescription(chaosGameDescription);
    assertEquals(chaosGameDescription, ChaosGame.getInstance().getDescription());
  }

  /**
   * Tests getting steps that are allowed.
   * Verifies that the steps are the same as the ones that were set.
   */
  @Test
  public void getStepsPositiveTest() {
    ChaosGame.getInstance().setSteps(100);
    assertEquals(100, ChaosGame.getInstance().getSteps());
  }

  /**
   * Tests getting out of bounds count that is allowed.
   * Verifies that the out of bounds count is 0.
   */
  @Test
  public void getOutOfBoundsCountPositiveTest() {
    assertEquals(0, ChaosGame.getInstance().getOutOfBoundsCount());
  }

  /**
   * Tests getting observers that is allowed.
   * Verifies that the observers list size is 1.
   */
  @Test
  public void getObserversPositiveTest() {
    GameController gameController = new GameController();
    CanvasController canvasController = new CanvasController();
    FileController fileController = new FileController();
    View view = new View(gameController, canvasController, fileController);
    ChaosGame.getInstance().addObserver(view);
    assertEquals(1, ChaosGame.getInstance().getObservers().size());
  }
}
