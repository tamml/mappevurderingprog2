package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.JuliaTransform;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * The JuliaTransformPositiveTest class contains positive test cases
 * for the JuliaTransform class.
 */
public class JuliaTransformPositiveTest {

  /**
   * Tests the juliaTransformConstructor method of JuliaTransform with positive input.
   * Verifies that the point and sign are not null and 0 respectively.
   */
  @Test
  public void juliaTransformConstructorPositiveTest() {
    Complex complex = new Complex(1, 1);
    int i = 1;
    JuliaTransform juliaTransform = new JuliaTransform(complex, i);
    System.out.println(juliaTransform.getPoint().getRealPart() + " " + juliaTransform.getPoint().getImaginaryPart() + " " + juliaTransform.getSign());
  }

  /**
   * Tests the getPoint method of JuliaTransform with positive input.
   * Verifies that the real and imaginary part of the point are not 2.
   */
  @Test
  public void getPointPositiveTest() {
    Complex complex = new Complex(1, 1);
    int i = 1;
    JuliaTransform juliaTransform = new JuliaTransform(complex, i);
    System.out.println(juliaTransform.getPoint().getRealPart() + " " + juliaTransform.getPoint().getImaginaryPart());
  }

  /**
   * Tests the getSign method of JuliaTransform with positive input.
   * Verifies that the sign is not 2.
   */
  @Test
  public void getSignPositiveTest() {
    Complex complex = new Complex(1, 1);
    int i = 1;
    JuliaTransform juliaTransform = new JuliaTransform(complex, i);
    System.out.println(juliaTransform.getSign());
  }

  /**
   * Tests the transform method of JuliaTransform with positive input.
   * Verifies that the result is not null and an instance of Complex.
   */
  @Test
  public void transformPositiveTest() {
    Complex complexToTransform = new Complex(1, 1);
    Complex juliaPoint = new Complex(0.5, -0.5);
    int sign = 1;
    JuliaTransform juliaTransform = new JuliaTransform(juliaPoint, sign);
    Vector2D result = juliaTransform.transform(complexToTransform);

    Assertions.assertNotNull(result);
    Assertions.assertTrue(result instanceof Complex);
  }
}