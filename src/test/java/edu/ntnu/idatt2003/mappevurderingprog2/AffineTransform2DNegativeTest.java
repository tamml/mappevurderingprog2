package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The AffineTransform2DNegativeTest class contains negative test cases for the AffineTransform2D class.
 */
public class AffineTransform2DNegativeTest {

  /**
   * Tests the AffineTransform2D constructor with a null matrix argument.
   */
  @Test
  public void affineTransform2DConstructorNegativeTest1() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    try {
      AffineTransform2D transform = new AffineTransform2D(null, vector);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Tests the AffineTransform2D constructor with a null vector argument.
   */
  @Test
  public void affineTransform2DConstructorNegativeTest2() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    try {
      AffineTransform2D transform = new AffineTransform2D(matrix, null);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Tests the getMatrix method with incorrect assertions.
   */
  @Test
  public void getMatrixNegativeTest() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    assert transform.getMatrix().getA00() != 2;
    assert transform.getMatrix().getA01() != 2;
    assert transform.getMatrix().getA10() != 2;
    assert transform.getMatrix().getA11() != 2;
  }

  /**
   * Tests the getVector method with incorrect assertions.
   */
  @Test
  public void getVectorNegativeTest() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    assert transform.getVector().getX0() != 2;
    assert transform.getVector().getX1() != 2;
  }

  /**
   * Tests the transform method with a null vector argument.
   */
  @Test
  public void transformNegativeTest() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    try {
      Vector2D result = transform.transform(null);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }
}
