package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosCanvas;
import org.junit.jupiter.api.Test;

/**
 * The ChaosCanvasPositiveTest class contains positive test cases for the ChaosCanvas class.
 */
public class ChaosCanvasPositiveTest {

  /**
   * Tests the ChaosCanvas constructor with valid input.
   */
  @Test
  public void chaosCanvasConstructorPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assertNotNull(chaosCanvas);
  }

  /**
   * Tests the getMinCoords method with valid input.
   */
  @Test
  public void getMinCoordsPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assertEquals(chaosCanvas.getMinCoords().getX0(), 0);
    assertEquals(chaosCanvas.getMinCoords().getX1(), 0);
  }

  /**
   * Tests the getMaxCoords method with valid input.
   */
  @Test
  public void getMaxCoordsPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assertEquals(chaosCanvas.getMaxCoords().getX0(), 1);
    assertEquals(chaosCanvas.getMaxCoords().getX1(), 1);
  }

  /**
   * Tests the getPixel method with valid input.
   */
  @Test
  public void getPixelPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    chaosCanvas.putPixel(new Vector2D(0.5, 0.5));
    assertEquals(chaosCanvas.getPixel(new Vector2D(0.5, 0.5)), 1);
  }

  /**
   * Tests the putPixel method with valid input.
   */
  @Test
  public void putPixelPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    chaosCanvas.putPixel(new Vector2D(0.5, 0.5));
    assertEquals(chaosCanvas.getPixel(new Vector2D(0.5, 0.5)), 1);
  }

  /**
   * Tests the getCanvasArray method with valid input.
   */
  @Test
  public void getCanvasArrayPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assertNotNull(chaosCanvas.getCanvasArray());
  }

  /**
   * Tests the clear method with valid input.
   */
  @Test
  public void clearPositiveTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    chaosCanvas.putPixel(new Vector2D(0.5, 0.5));
    chaosCanvas.clear();
    assertEquals(chaosCanvas.getPixel(new Vector2D(0.5, 0.5)), 0);
  }
}
