package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The AffineTransform2DPositiveTest class contains positive test cases for the AffineTransform2D class.
 */
public class AffineTransform2DPositiveTest {

  /**
   * Tests the AffineTransform2D constructor with positive input.
   */
  @Test
  public void affineTransform2DConstructorPositiveTest() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    assertEquals(matrix, transform.getMatrix());
    assertEquals(vector, transform.getVector());
  }

  /**
   * Tests the getMatrix method with positive input.
   */
  @Test
  public void getMatrix() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    assertEquals(matrix, transform.getMatrix());
  }

  /**
   * Tests the getVector method with positive input.
   */
  @Test
  public void getVector() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    assertEquals(vector, transform.getVector());
  }

  /**
   * Tests the transform method with positive input.
   */
  @Test
  public void transform() {
    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(1, 1);
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    Vector2D point = new Vector2D(1, 1);
    Vector2D result = transform.transform(point);
    assertEquals(2, result.getX0());
    assertEquals(2, result.getX1());
  }
}
