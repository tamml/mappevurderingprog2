package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameDescriptionPositiveTest class contains positive test cases for the ChaosGameDescription class.
 */
public class ChaosGameDescriptionPositiveTest {

  /**
   * Tests the ChaosGameDescription constructor without weighted transforms with positive input.
   * Verifies that the created ChaosGameDescription instance matches the expected parameters.
   */
  @Test
  public void chaosGameDescriptionConstructorWithoutWeightedPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription
        chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    assertEquals(transforms, chaosGameDescription.getTransforms());
    assertEquals(minCoords, chaosGameDescription.getMinCoords());
    assertEquals(maxCoords, chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the ChaosGameDescription constructor with weighted transforms with positive input.
   * Verifies that the created ChaosGameDescription instance matches the expected parameters.
   */
  @Test
  public void chaosGameDescriptionConstructorWithWeightedPositiveTest() {
    List<Pair<Transform2D, Double>> weightedTransforms = new ArrayList<>();
    weightedTransforms.add(
        new Pair<>(new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)), 0.01));
    weightedTransforms.add(new Pair<>(
        new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6)), 0.85));
    weightedTransforms.add(new Pair<>(
        new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6)), 0.07));
    weightedTransforms.add(new Pair<>(
        new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44)),
        0.07));
    Vector2D minCoords = new Vector2D(-3, 0);
    Vector2D maxCoords = new Vector2D(3, 10);
    ChaosGameDescription barnsleyFern =
        new ChaosGameDescription(weightedTransforms, minCoords, maxCoords, true);
    assertEquals(weightedTransforms, barnsleyFern.getWeightedTransforms());
    assertEquals(
        weightedTransforms.stream().map(Pair::getKey).collect(java.util.stream.Collectors.toList()),
        barnsleyFern.getTransforms());
    assertEquals(minCoords, barnsleyFern.getMinCoords());
    assertEquals(maxCoords, barnsleyFern.getMaxCoords());
  }

  /**
   * Tests the getTransforms method of ChaosGameDescription with positive input.
   */
  @Test
  public void getTransformsPositiveTest() {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(100, 100));
    assertEquals(transforms, chaosGameDescription.getTransforms());
  }

  /**
   * Tests the getWeightedTransforms method of ChaosGameDescription with positive input.
   */
  @Test
  public void getWeightedTransformsPositiveTest() {
    List<Pair<Transform2D, Double>> weightedTransforms = new ArrayList<>();
    weightedTransforms.add(
        new Pair<>(new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)), 0.01));
    weightedTransforms.add(new Pair<>(
        new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6)), 0.85));
    weightedTransforms.add(new Pair<>(
        new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6)), 0.07));
    weightedTransforms.add(new Pair<>(
        new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44)),
        0.07));
    ChaosGameDescription barnsleyFern =
        new ChaosGameDescription(weightedTransforms, new Vector2D(-3, 0), new Vector2D(3, 10),
            true);
    assertEquals(weightedTransforms, barnsleyFern.getWeightedTransforms());
  }

  /**
   * Tests the getMinCoords method of ChaosGameDescription with positive input.
   */
  @Test
  public void getMinCoordsPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), minCoords, new Vector2D(100, 100));
    assertEquals(minCoords, chaosGameDescription.getMinCoords());
  }

  /**
   * Tests the getMaxCoords method of ChaosGameDescription with positive input.
   */
  @Test
  public void getMaxCoordsPositiveTest() {
    Vector2D maxCoords = new Vector2D(100, 100);
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), maxCoords);
    assertEquals(maxCoords, chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the isWeighted method of ChaosGameDescription with positive input.
   */
  @Test
  public void isWeightedPositiveTest() {
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
    assertEquals(false, chaosGameDescription.isWeighted());
  }

  /**
   * Tests the setMinCoords method of ChaosGameDescription with positive input.
   */
  @Test
  public void setMinCoordsPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
    chaosGameDescription.setMinCoords(minCoords);
    assertEquals(minCoords, chaosGameDescription.getMinCoords());
  }

  /**
   * Tests the setMaxCoords method of ChaosGameDescription with positive input.
   */
  @Test
  public void setMaxCoordsPositiveTest() {
    Vector2D maxCoords = new Vector2D(100, 100);
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
    chaosGameDescription.setMaxCoords(maxCoords);
    assertEquals(maxCoords, chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the getInitialMinCoords method of ChaosGameDescription with positive input.
   */
  @Test
  public void getInitialMinCoordsPositiveTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), minCoords, new Vector2D(100, 100));
    assertEquals(minCoords.getX0(), chaosGameDescription.getInitialMinCoords().getX0());
    assertEquals(minCoords.getX1(), chaosGameDescription.getInitialMinCoords().getX1());
  }

  /**
   * Tests the getInitialMaxCoords method of ChaosGameDescription with positive input.
   */
  @Test
  public void getInitialMaxCoordsPositiveTest() {
    Vector2D maxCoords = new Vector2D(100, 100);
    ChaosGameDescription chaosGameDescription =
        new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), maxCoords);
    assertEquals(maxCoords.getX0(), chaosGameDescription.getInitialMaxCoords().getX0());
    assertEquals(maxCoords.getX1(), chaosGameDescription.getInitialMaxCoords().getX1());
  }
}