package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The Matrix2x2NegativeTest class contains negative test cases for the Matrix2x2 class.
 */
public class Matrix2x2NegativeTest {
  /**
   * Tests the matrix2x2Constructor method of Matrix2x2 with negative input.
   * Verifies that the matrix is not initialized with all 1's.
   */
  @Test
  public void matrix2x2ConstructorNegativeTest() {
    Matrix2x2 matrix1 = new Matrix2x2(0, 0, 0, 0);
    assert matrix1.getA00() != 1;
    assert matrix1.getA01() != 1;
    assert matrix1.getA10() != 1;
    assert matrix1.getA11() != 1;
  }

  /**
   * Tests the matrix2x2GetA00 method of Matrix2x2 with negative input.
   * Verifies that the value of a00 is not 0.
   */
  @Test
  public void matrix2x2GetA00NegativeTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    assert matrix1.getA00() != 0;
  }

  /**
   * Tests the matrix2x2GetA01 method of Matrix2x2 with negative input.
   * Verifies that the value of a01 is not 0.
   */
  @Test
  public void matrix2x2GetA01NegativeTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    assert matrix1.getA01() != 0;
  }

  /**
   * Tests the matrix2x2GetA10 method of Matrix2x2 with negative input.
   * Verifies that the value of a10 is not 0.
   */
  @Test
  public void matrix2x2GetA10NegativeTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    assert matrix1.getA10() != 0;
  }

  /**
   * Tests the matrix2x2GetA11 method of Matrix2x2 with negative input.
   * Verifies that the value of a11 is not 0.
   */
  @Test
  public void matrix2x2GetA11NegativeTest() {
    Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
    assert matrix1.getA11() != 0;
  }

  /**
   * Tests the matrix2x2Multiply method of Matrix2x2 with negative input.
   * Verifies that an IllegalArgumentException is thrown when the vector to multiply is null.
   */
  @Test
  public void matrix2x2MultiplyNegativeTest() {
    try {
      Matrix2x2 matrix1 = new Matrix2x2(1, 1, 1, 1);
      Vector2D result = matrix1.multiply(null);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }
}