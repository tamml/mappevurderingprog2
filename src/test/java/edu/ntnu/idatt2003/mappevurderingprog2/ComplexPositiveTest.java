package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import org.junit.jupiter.api.Test;

/**
 * The ComplexPositiveTest class contains positive test cases for the Complex class.
 */
public class ComplexPositiveTest {

  /**
   * Tests the complexConstructor method of Complex with positive input.
   * Verifies that the real part of the complex number is 1.
   */
  @Test
  public void complexConstructorPositiveTest() {
    Complex complex1 = new Complex(1, 1);
    assertEquals(1, complex1.getRealPart());
    assertEquals(1, complex1.getImaginaryPart());
  }

  /**
   * Tests the getRealPart method of Complex with positive input.
   * Verifies that the real part of the complex number is 2.
   */
  @Test
  public void getRealPartPositiveTest() {
    Complex complex1 = new Complex(2, 1);
    assertEquals(2, complex1.getRealPart());
  }

  /**
   * Tests the getImaginaryPart method of Complex with positive input.
   * Verifies that the imaginary part of the complex number is 2.
   */
  @Test
  public void getImaginaryPartPositiveTest() {
    Complex complex1 = new Complex(1, 2);
    assertEquals(2, complex1.getImaginaryPart());

  }

  /**
   * Tests the add method of Complex with positive input.
   * Verifies that the real part of the result is 3.
   */
  @Test
  public void addPositiveTest() {
    Complex complex1 = new Complex(1, 1);
    Complex complex2 = new Complex(2, 2);
    Complex result = complex1.add(complex2);
    assertEquals(3, result.getRealPart());
    assertEquals(3, result.getImaginaryPart());
  }

  /**
   * Tests the subtract method of Complex with positive input.
   * Verifies that the real part of the result is 1.
   */
  @Test
  public void subtractPositiveTest() {
    Complex complex1 = new Complex(2, 2);
    Complex complex2 = new Complex(1, 1);
    Complex result = complex1.subtract(complex2);
    assertEquals(1, result.getRealPart());
    assertEquals(1, result.getImaginaryPart());
  }

  /**
   * Tests the multiply method of Complex with positive input.
   * Verifies that the real part of the result is 0.
   */
  @Test
  public void sqrtPositiveTest() {
    Complex complex1 = new Complex(4, 4);
    Complex result = complex1.sqrt();
    assertEquals(2.1973682269356196, result.getRealPart());
    assertEquals(0.9101797211244547, result.getImaginaryPart());
  }
}