package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.JuliaTransform;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The JuliaTransformNegativeTest class contains negative test cases for the JuliaTransform class.
 */
public class JuliaTransformNegativeTest {

  /**
   * Tests the juliaTransformConstructor method of JuliaTransform with negative input.
   * Verifies that an IllegalArgumentException is thrown when the point is null.
   */
  @Test
  public void juliaTransformConstructorNegativeTest() {
    int i = 1;
    assertThrows(IllegalArgumentException.class, () -> {
      JuliaTransform juliaTransform = new JuliaTransform(null, i);
    });
  }

  /**
   * Tests the juliaTransformConstructor method of JuliaTransform with negative input.
   * Verifies that the point and sign are not null and 0 respectively.
   */
  @Test
  public void juliaTransformConstructorNegativeTest2() {
    Complex complex = new Complex(1, 1);
    int i = 1;
    JuliaTransform juliaTransform = new JuliaTransform(complex, i);
    assert juliaTransform.getPoint() != null;
    assert juliaTransform.getSign() != 0;
  }

  /**
   * Tests the getPoint method of JuliaTransform with negative input.
   * Verifies that the real and imaginary part of the point are not 2.
   */
  @Test
  public void getPointNegativeTest() {
    Complex complex = new Complex(1, 1);
    int i = 1;
    JuliaTransform juliaTransform = new JuliaTransform(complex, i);
    assert juliaTransform.getPoint().getRealPart() != 2;
    assert juliaTransform.getPoint().getImaginaryPart() != 2;
  }

  /**
   * Tests the getSign method of JuliaTransform with negative input.
   * Verifies that the sign is not 2.
   */
  @Test
  public void getSignNegativeTest() {
    Complex complex = new Complex(1, 1);
    int i = 1;
    JuliaTransform juliaTransform = new JuliaTransform(complex, i);
    assert juliaTransform.getSign() != 2;
  }

  /**
   * Tests the transform method of JuliaTransform with negative input.
   * Verifies that an IllegalArgumentException is thrown when the complex number to transform is null.
   */
  @Test
  public void transformNegativeTest1() {
    Complex juliaPoint = new Complex(0.5, -0.5);
    int sign = 1;
    JuliaTransform juliaTransform = new JuliaTransform(juliaPoint, sign);
    assertThrows(IllegalArgumentException.class, () -> {
      juliaTransform.transform(null);
    });
  }

  /**
   * Tests the transform method of JuliaTransform with negative input.
   * Verifies that an IllegalArgumentException is thrown when the complex number to transform is a Vector2D.
   */
  @Test
  public void transformNegativeTest2() {
    Vector2D complexToTransform = new Vector2D(1, 1);
    Complex juliaPoint = new Complex(0.5, -0.5);
    int sign = 1;
    JuliaTransform juliaTransform = new JuliaTransform(juliaPoint, sign);
    assertThrows(IllegalArgumentException.class, () -> {
      juliaTransform.transform(complexToTransform);
    });
  }

  /**
   * Tests the transform method of JuliaTransform with negative input.
   * Verifies that the transformed complex number is not the same as the input complex number.
   */
  @Test
  public void transformNegativeTest3() {
    Complex complexToTransform = new Complex(1, 1);
    Complex juliaPoint = new Complex(0.5, -0.5);
    int sign = 1;
    JuliaTransform juliaTransform = new JuliaTransform(juliaPoint, sign);
    Vector2D result = juliaTransform.transform(complexToTransform);
    assert result.getX0() != 1;
    assert result.getX1() != 1;
  }

  /**
   * Tests the transform method of JuliaTransform with negative input.
   * Verifies that the transformed complex number is not the same as the input complex number.
   */
  @Test
  public void transformNegativeTest4() {
    Complex complexToTransform = new Complex(1, 1);
    Complex juliaPoint = new Complex(0.5, -0.5);
    int sign = -1;
    JuliaTransform juliaTransform = new JuliaTransform(juliaPoint, sign);
    Vector2D result = juliaTransform.transform(complexToTransform);
    assert result.getX0() != 1;
    assert result.getX1() != 1;
  }
}