package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescriptionFactory;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameDescriptionFactoryNegativeTest class contains negative test cases
 * for the ChaosGameDescriptionFactory class.
 */
public class ChaosGameDescriptionFactoryNegativeTest {

  /**
   * Tests the createSierpinskiTriangle method of
   * ChaosGameDescriptionFactory with negative input.
   */
  @Test
  public void createSierpinskiTriangleNegativeTest() {
    ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createSierpinskiTriangle();
    assert chaosGameDescription.getTransforms().size() != 0;
  }

  /**
   * Tests the createBarnsleyFern method of
   * ChaosGameDescriptionFactory with negative input.
   */
  @Test
  public void createBarnsleyFernNegativeTest() {
    ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createBarnsleyFern();
    assert chaosGameDescription.getWeightedTransforms().size() != 0;
  }

  /**
   * Tests the createStandardJuliaTransformation method of ChaosGameDescriptionFactory with negative input.
   */
  @Test
  public void createStandardJuliaTransformationNegativeTest() {
    ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createStandardJuliaTransformation();
    assert chaosGameDescription.getTransforms().size() != 0;
  }
}
