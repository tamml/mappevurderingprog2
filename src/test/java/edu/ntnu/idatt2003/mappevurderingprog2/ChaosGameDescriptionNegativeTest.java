package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;

import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameDescriptionNegativeTest class contains negative test cases for the ChaosGameDescription class.
 */
public class ChaosGameDescriptionNegativeTest {

  /**
   * Tests the ChaosGameDescription constructor without weighted transforms with negative input.
   * Verifies that the created ChaosGameDescription instance does not match the expected parameters.
   */
  @Test
  public void chaosGameDescriptionConstructorWithoutWeightedNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    Vector2D minCoords1 = new Vector2D(1, 1);
    Vector2D maxCoords1 = new Vector2D(101, 101);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(1, 1, 1, 1), new Vector2D(1, 1)));
    List<Transform2D> transforms1 = new ArrayList<>();
    transforms1.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription
        chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    assert (transforms1 != chaosGameDescription.getTransforms());
    assert (minCoords1 != chaosGameDescription.getMinCoords());
    assert (maxCoords1 != chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the ChaosGameDescription constructor with weighted transforms with negative input.
   * Verifies that the created ChaosGameDescription instance does not match the expected parameters.
   */
  @Test
  public void chaosGameDescriptionConstructorWithWeightedNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    Vector2D minCoords1 = new Vector2D(1, 1);
    Vector2D maxCoords1 = new Vector2D(101, 101);
    List<Pair<Transform2D, Double>> weightedTransforms = new ArrayList<>();
    weightedTransforms.add(new Pair<>(new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)), 0.01));
    List<Pair<Transform2D, Double>> weightedTransforms1 = new ArrayList<>();
    weightedTransforms1.add(new Pair<>(new AffineTransform2D(new Matrix2x2(1, 1, 1, 1), new Vector2D(1, 1)), 0.02));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(weightedTransforms, minCoords, maxCoords, true);
    assert (weightedTransforms1 != chaosGameDescription.getWeightedTransforms());
    assert (weightedTransforms1.stream().map(Pair::getKey).collect(java.util.stream.Collectors.toList()) != chaosGameDescription.getTransforms());
    assert (minCoords1 != chaosGameDescription.getMinCoords());
    assert (maxCoords1 != chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the getTransforms() method with negative input.
   * Verifies that the returned list of transforms does not match the expected value.
   */
  @Test
  public void getTransformsNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    Vector2D minCoords1 = new Vector2D(1, 1);
    Vector2D maxCoords1 = new Vector2D(101, 101);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    List<Transform2D> transforms1 = new ArrayList<>();
    transforms1.add(new AffineTransform2D(new Matrix2x2(1, 1, 1, 1), new Vector2D(1, 1)));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    assert (transforms1 != chaosGameDescription.getTransforms());
  }

  /**
   * Tests the getWeightedTransforms() method with negative input.
   * Verifies that the returned list of weighted transforms does not match the expected value.
   */
  @Test
  public void getWeightedTransformsTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    Vector2D minCoords1 = new Vector2D(1, 1);
    Vector2D maxCoords1 = new Vector2D(101, 101);
    List<Pair<Transform2D, Double>> weightedTransforms = new ArrayList<>();
    weightedTransforms.add(new Pair<>(new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)), 0.01));
    List<Pair<Transform2D, Double>> weightedTransforms1 = new ArrayList<>();
    weightedTransforms1.add(new Pair<>(new AffineTransform2D(new Matrix2x2(1, 1, 1, 1), new Vector2D(1, 1)), 0.02));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(weightedTransforms, minCoords, maxCoords, true);
    assert (weightedTransforms1 != chaosGameDescription.getWeightedTransforms());
  }

  /**
   * Tests the setTransforms() method with negative input.
   * Verifies that the set transforms do not match the expected value.
   */
  @Test
  public void getMinCoordsNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D minCoords1 = new Vector2D(1, 1);
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), minCoords, new Vector2D(100, 100));
    assert (minCoords1 != chaosGameDescription.getMinCoords());
  }

  /**
   * Tests the getMaxCoords() method with negative input.
   * Verifies that the returned maxCoords does not match the expected value.
   */
  @Test
  public void getMaxCoordsNegativeTest() {
    Vector2D maxCoords = new Vector2D(100, 100);
    Vector2D maxCoords1 = new Vector2D(101, 101);
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), maxCoords);
    assert (maxCoords1 != chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the isWeighted() method with negative input.
   * Verifies that the returned value does not match the expected value.
   */
  @Test
  public void isWeightedNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), minCoords, maxCoords);
    assert (!chaosGameDescription.isWeighted());
  }

  /**
   * Tests the setMinCoords() method with negative input.
   * Verifies that the set minCoords do not match the expected value.
   */
  @Test
  public void setMinCoordsNegativeTest1() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D minCoords1 = new Vector2D(1, 1);
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), minCoords, new Vector2D(100, 100));
    chaosGameDescription.setMinCoords(minCoords1);
    assert (minCoords != chaosGameDescription.getMinCoords());
  }

  /**
   * Tests the setMinCoords() method with negative input.
   * Verifies that the set minCoords do not match the expected value.
   */
  @Test
  public void setMinCoordsNegativeTest2() {
    assertThrows(IllegalArgumentException.class, () -> {
      ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
      chaosGameDescription.setMinCoords(null);
    });
  }

  /**
   * Tests the setMinCoords() method with negative input.
   * Verifies that the set minCoords do not match the expected value.
   */
  @Test
  public void setMinCoordsNegativeTest3() {
    assertThrows(IllegalArgumentException.class, () -> {
      ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
      chaosGameDescription.setMinCoords(new Vector2D(100, 100));
    });
  }

  /**
   * Tests the setMaxCoords() method with negative input.
   * Verifies that the set maxCoords do not match the expected value.
   */
  @Test
  public void setMaxCoordsNegativeTest1() {
    Vector2D maxCoords = new Vector2D(100, 100);
    Vector2D maxCoords1 = new Vector2D(101, 101);
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), maxCoords);
    chaosGameDescription.setMaxCoords(maxCoords1);
    assert (maxCoords != chaosGameDescription.getMaxCoords());
  }

  /**
   * Tests the setMaxCoords() method with negative input.
   * Verifies that the set maxCoords do not match the expected value.
   */
  @Test
  public void setMaxCoordsNegativeTest2() {
    assertThrows(IllegalArgumentException.class, () -> {
      ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
      chaosGameDescription.setMaxCoords(null);
    });
  }

  /**
   * Tests the setMaxCoords() method with negative input.
   * Verifies that the set maxCoords do not match the expected value.
   */
  @Test
  public void setMaxCoordsNegativeTest3() {
    assertThrows(IllegalArgumentException.class, () -> {
      ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), new Vector2D(100, 100));
      chaosGameDescription.setMaxCoords(new Vector2D(0, 0));
    });
  }
}
