package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescriptionFactory;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameDescriptionFactoryPositiveTest class contains positive test cases for the ChaosGameDescriptionFactory class.
 */
public class ChaosGameDescriptionFactoryPositiveTest {

  /**
   * Tests the createSierpinskiTriangle method of ChaosGameDescriptionFactory with positive input.
   * Verifies that the generated chaos game description contains 3 transforms.
   */
  @Test
  public void createSierpinskiTrianglePositiveTest() {
    ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createSierpinskiTriangle();
    assertEquals(3, chaosGameDescription.getTransforms().size());
  }

  /**
   * Tests the createBarnsleyFern method of ChaosGameDescriptionFactory with positive input.
   * Verifies that the generated chaos game description contains 4 weighted transforms.
   */
  @Test
  public void createBarnsleyFernPositiveTest() {
    ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createBarnsleyFern();
    assertEquals(4, chaosGameDescription.getWeightedTransforms().size());
  }

  /**
   * Tests the createStandardJuliaTransformation method of ChaosGameDescriptionFactory with positive input.
   * Verifies that the generated chaos game description contains 2 transforms.
   */
  @Test
  public void createStandardJuliaTransformationPositiveTest() {
    ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createStandardJuliaTransformation();
    assertEquals(2, chaosGameDescription.getTransforms().size());
  }
}
