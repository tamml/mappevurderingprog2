package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import org.junit.jupiter.api.Test;

/**
 * The ComplexNegativeTest class contains negative test cases for the Complex class.
 */
public class ComplexNegativeTest {

  /**
   * Tests the complexConstructor method of Complex with negative input.
   * Verifies that the real part of the complex number is not 2.
   */
  @Test
  public void complexConstructorNegativeTest() {
    Complex complex1 = new Complex(1, 1);
    assert complex1.getRealPart() != 2;
  }

  /**
   * Tests the getRealPart method of Complex with negative input.
   * Verifies that the real part of the complex number is not 1.
   */
  @Test
  public void getRealPartNegativeTest() {
    Complex complex1 = new Complex(2, 1);
    assert complex1.getRealPart() != 1;
  }

  /**
   * Tests the getImaginaryPart method of Complex with negative input.
   * Verifies that the imaginary part of the complex number is not 2.
   */
  @Test
  public void getImaginaryPartNegativeTest() {
    Complex complex1 = new Complex(1, 2);
    assert complex1.getImaginaryPart() != 1;
  }

  /**
   * Tests the add method of Complex with negative input.
   * Verifies that the real part of the result is not 2.
   */
  @Test
  public void addNegativeTest() {
    Complex complex1 = new Complex(1, 1);
    Complex complex2 = new Complex(1, 1);
    Complex result = complex1.add(complex2);
    assert result.getRealPart() != 1;
    assert result.getImaginaryPart() != 1;
  }

  /**
   * Tests the add method of Complex with negative input.
   * Verifies that an IllegalArgumentException is
   * thrown when adding a Complex number and a Vector2D.
   */
  @Test
  public void addNegativeTest2() {
    Complex complex = new Complex(1, 2);
    Vector2D vector = new Vector2D(3, 4);

    assertThrows(IllegalArgumentException.class, () -> {
      complex.add(vector);
    });
  }

  /**
   * Tests the subtract method of Complex with negative input.
   * Verifies that the real part of the result is not 1.
   */
  @Test
  public void subtractNegativeTest() {
    Complex complex1 = new Complex(1, 1);
    Complex complex2 = new Complex(1, 1);
    Complex result = complex1.subtract(complex2);
    assert result.getRealPart() != 1;
    assert result.getImaginaryPart() != 1;
  }

  /**
   * Tests the subtract method of Complex with negative input.
   * Verifies that an IllegalArgumentException is
   * thrown when subtracting a Complex number and a Vector2D.
   */
  @Test
  public void subtractNegativeTest2() {
    Complex complex = new Complex(1, 2);
    Vector2D vector = new Vector2D(3, 4);

    assertThrows(IllegalArgumentException.class, () -> {
      complex.subtract(vector);
    });
  }

  /**
   * Tests the multiply method of Complex with negative input.
   * Verifies that the real part of the result is not 2.
   */
  @Test
  public void sqrtNegativeTest() {
    Complex complex1 = new Complex(4, 4);
    Complex result = complex1.sqrt();
    assert result.getRealPart() != 2;
    assert result.getImaginaryPart() != 2;
  }
}