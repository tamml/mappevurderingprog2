package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertFalse;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosCanvas;
import org.junit.jupiter.api.Test;

/**
 * The ChaosCanvasNegativeTest class contains negative test cases for the ChaosCanvas class.
 */
public class ChaosCanvasNegativeTest {

  /**
   * Tests the ChaosCanvas constructor with negative input.
   */
  @Test
  public void ChaosCanvasConstructorNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assert chaosCanvas.getMinCoords().getX0() != 1;
    assert chaosCanvas.getMinCoords().getX1() != 1;
    assert chaosCanvas.getMaxCoords().getX1() != 0;
    assert chaosCanvas.getMaxCoords().getX0() != 0;
  }

  /**
   * Tests the getMinCoords method with negative input.
   */
  @Test
  public void getMinCoordsNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assert chaosCanvas.getMinCoords().getX0() != 1;
    assert chaosCanvas.getMinCoords().getX1() != 1;
  }

  /**
   * Tests the getMaxCoords method with negative input.
   */
  @Test
  public void getMaxCoordsNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assert chaosCanvas.getMaxCoords().getX0() != 0;
    assert chaosCanvas.getMaxCoords().getX1() != 0;
  }

  /**
   * Tests the getPixel method with negative input.
   */
  @Test
  public void getPixelNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    chaosCanvas.putPixel(new Vector2D(0.5, 0.5));
    assert chaosCanvas.getPixel(new Vector2D(0.5, 0.5)) != 0;
  }

  /**
   * Tests the putPixel method with negative input.
   */
  @Test
  public void putPixelNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    assertFalse(chaosCanvas.putPixel(new Vector2D(100, 100)));
  }

  /**
   * Tests the getCanvasArray method with negative input.
   */
  @Test
  public void getCanvasArrayNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    chaosCanvas.getCanvasArray()[0][0] = 1;
    assert chaosCanvas.getCanvasArray()[0][0] != 0;
  }

  /**
   * Tests the clear method with negative input.
   */
  @Test
  public void clearNegativeTest() {
    ChaosCanvas chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
    chaosCanvas.putPixel(new Vector2D(0.5, 0.5));
    chaosCanvas.clear();
    assert chaosCanvas.getPixel(new Vector2D(0.5, 0.5)) != 1;
  }
}
