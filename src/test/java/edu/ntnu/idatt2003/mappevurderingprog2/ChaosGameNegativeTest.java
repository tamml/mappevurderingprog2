package edu.ntnu.idatt2003.mappevurderingprog2;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.CanvasController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGame;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.views.View;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * The ChaosGameNegativeTest class contains negative test cases for the ChaosGame class.
 */
public class ChaosGameNegativeTest {

  /**
   * Resets the ChaosGame instance before each test.
   */
  @BeforeEach
  public void tearDown() {
    ChaosGame.getInstance().setDescription(null);
    ChaosGame.getInstance().resetOutOfBoundsCount();
    ChaosGame.getInstance().getObservers().clear();
    ChaosGame.getInstance().setSteps(0);
  }

  /**
   * Tests setting a description that is not allowed.
   * Verifies that the description is not the same
   * as the one that was attempted to be set.
   */
  @Test
  public void setDescriptionNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(transforms, minCoords, maxCoords);
    Vector2D minCoords2 = new Vector2D(0, 0);
    Vector2D maxCoords2 = new Vector2D(100, 100);
    List<Transform2D> transforms2 = new ArrayList<>();
    transforms2.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription chaosGameDescription2 = new ChaosGameDescription(transforms2, minCoords2, maxCoords2);
    ChaosGame.getInstance().setDescription(chaosGameDescription);
    ChaosGame.getInstance().setDescription(chaosGameDescription2);
    assert ChaosGame.getInstance().getDescription() != chaosGameDescription;
  }

  /**
   * Tests setting a canvas that is not allowed.
   * Verifies that an IllegalStateException is thrown.
   */
  @Test
  public void setCanvasNegativeTest() {
    ChaosGame.getInstance().setDescription(null);
    assertThrows(IllegalStateException.class, () -> ChaosGame.getInstance().setCanvas());
  }

  /**
   * Tests setting steps that are not allowed.
   * Verifies that an IllegalArgumentException is thrown.
   */
  @Test
  public void setStepsNegativeTest() {
    assertThrows(IllegalArgumentException.class, () -> ChaosGame.getInstance().setSteps(-1));
  }

  /**
   * Tests adding an observer that is not allowed.
   * Verifies that an IllegalArgumentException is thrown.
   */
  @Test
  public void addObserverNegativeTest() {
    assertThrows(IllegalArgumentException.class, () -> ChaosGame.getInstance().addObserver(null));
  }

  /**
   * Tests removing an observer that is not allowed.
   * Verifies that an IllegalArgumentException is thrown.
   */
  @Test
  public void removeObserverNegativeTest() {
    assertThrows(IllegalArgumentException.class, () -> ChaosGame.getInstance().removeObserver(null));
  }

  /**
   * Tests notifying observers that is not allowed.
   * Verifies that an IllegalStateException is thrown.
   */
  @Test
  public void getCanvasNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGame.getInstance().setDescription(new ChaosGameDescription(transforms, minCoords, maxCoords));
    ChaosGame.getInstance().setCanvas();
    assert ChaosGame.getInstance().getCanvas() != null;
  }

  /**
   * Tests getting a description that is not allowed.
   * Verifies that the description is not the same
   * as the one that was attempted to be set.
   */
  @Test
  public void getDescriptionNegativeTest() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(100, 100);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, 0.5, 0.5), new Vector2D(0, 0)));
    ChaosGame.getInstance().setDescription(new ChaosGameDescription(transforms, minCoords, maxCoords));
    assert ChaosGame.getInstance().getDescription() != null;
  }

  /**
   * Tests getting steps that are not allowed.
   * Verifies that the steps are not the same as the ones that were set.
   */
  @Test
  public void getStepsNegativeTest() {
    ChaosGame.getInstance().setSteps(100);
    assert ChaosGame.getInstance().getSteps() != 101;
  }

  /**
   * Tests getting out of bounds count that is not allowed.
   * Verifies that the out of bounds count is not the same as the one that was set.
   */
  @Test
  public void getOutOfBoundsCountNegativeTest() {
    assert ChaosGame.getInstance().getOutOfBoundsCount() != 1;
  }

  /**
   * Tests getting observers that is not allowed.
   * Verifies that the observers are not the same as the ones that were set.
   */
  @Test
  public void getObserversNegativeTest() {
    GameController gameController = new GameController();
    CanvasController canvasController = new CanvasController();
    FileController fileController = new FileController();
    View view = new View(gameController, canvasController, fileController);
    ChaosGame.getInstance().addObserver(view);
    System.out.println(ChaosGame.getInstance().getObservers().size() + " observers");
    assert ChaosGame.getInstance().getObservers().size() != 0;
  }
}
