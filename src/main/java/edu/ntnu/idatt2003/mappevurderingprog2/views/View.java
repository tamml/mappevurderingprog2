package edu.ntnu.idatt2003.mappevurderingprog2.views;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.CanvasController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameObserver;
import edu.ntnu.idatt2003.mappevurderingprog2.utils.Colorpalette;
import edu.ntnu.idatt2003.mappevurderingprog2.utils.Size;
import edu.ntnu.idatt2003.mappevurderingprog2.views.components.ExtraUserOptions;
import edu.ntnu.idatt2003.mappevurderingprog2.views.components.Menu;
import java.io.FileNotFoundException;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

/**
 * The View class represents the main view of the application.
 * It contains the main canvas, menu, and extra user options.
 * It also implements the ChaosGameObserver interface to listen
 * for updates from the GameController.
 */
public class View extends BorderPane implements ChaosGameObserver {

  // Private Canvas object.
  private Canvas mainCanvas;

  // Private GameController object.
  private GameController gameController;

  // Private CanvasController object.
  private CanvasController canvasController;

  // Private FileController object.
  private FileController fileController;

  // Private Menu object.
  private Menu menu;

  // Private ExtraUserOptions object.
  private ExtraUserOptions extraUserOptions;

  /**
   * Constructs a new View object.
   *
   * @param gameController The GameController object associated with the View object.
   * @param canvasController The CanvasController object associated with the View object.
   * @param fileController The FileController object associated with the View object.
   */
  public View(GameController gameController, CanvasController canvasController,
              FileController fileController) {
    this.gameController = gameController;
    this.canvasController = canvasController;
    this.fileController = fileController;
    this.mainCanvas = new Canvas(600, 400);
  }

  /**
   * Retrieves the main canvas of the view.
   *
   * @return The main canvas.
   */
  public Canvas getMainCanvas() {
    return mainCanvas;
  }

  /**
   * Creates the scene for the view.
   *
   * @return The created scene.
   * @throws FileNotFoundException If a file is not found.
   */
  public Scene createScene() throws FileNotFoundException {
    this.setBackground(new Background(
        new BackgroundFill(Colorpalette.Primary, CornerRadii.EMPTY, Insets.EMPTY)));
    this.setCenter(mainCanvas);
    this.menu = new Menu(this, gameController, canvasController, fileController);
    menu.setPrefWidth(Size.getScreenWidth() * 0.15);
    menu.setMaxWidth(Size.getScreenWidth() * 0.15);
    initializeExtraUserOptions();
    this.setLeft(menu);
    return new Scene(this, Size.getScreenWidth(), Size.getScreenHeight());
  }

  /**
   * Initializes the extra user options.
   */
  private void initializeExtraUserOptions() {
    extraUserOptions = new ExtraUserOptions(this.getMainCanvas(), gameController);
    extraUserOptions.setPrefWidth(Size.getScreenWidth() * 0.15);
    extraUserOptions.setMaxWidth(Size.getScreenWidth() * 0.15);
    extraUserOptions.setPrefHeight(Size.getScreenHeight());
    extraUserOptions.setMaxHeight(Size.getScreenHeight());
    extraUserOptions.setBackground(new Background(new BackgroundFill(
        Color.rgb(238, 217, 196), CornerRadii.EMPTY, Insets.EMPTY)));
    extraUserOptions.setStyle("-fx-border-color: black; -fx-border-width: 0 0 0 2px;");
    this.setRight(extraUserOptions);
  }

  /**
   * Callback method triggered when the chaos game is updated.
   * This method updates the canvas display.
   */
  @Override
  public void onChaosGameUpdated() {
    Platform.runLater(() -> {
      canvasController.calculateQuantiles();
      mainCanvas = canvasController.updateCanvasDisplay(mainCanvas);
    });
  }
}
