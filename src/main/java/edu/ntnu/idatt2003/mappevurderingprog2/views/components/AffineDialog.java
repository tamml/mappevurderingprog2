package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * The AffineDialog class represents a dialog for editing
 * and displaying affine transformations.
 * It allows users to input matrix and vector values for affine transformations
 * and validate them.
 */
public class AffineDialog {

  // The dialog for editing affine transformations
  private Dialog<Void> dialog;

  // The game controller
  private GameController gameController;

  // A boolean indicating whether the dialog is in edit mode
  private boolean isEditMode;

  // The grid pane for the dialog
  private GridPane grid = new GridPane();

  /**
   * Constructs an AffineDialog object with the given game controller and edit mode flag.
   *
   * @param gameController the game controller to be used
   * @param isEditMode     flag indicating if the dialog is in edit mode
   */
  public AffineDialog(GameController gameController, boolean isEditMode) {
    this.gameController = gameController;
    this.isEditMode = isEditMode;
    setupDialog();
  }

  /**
   * Sets up the dialog components and layout.
   */
  private void setupDialog() {
    dialog = new Dialog<>();
    dialog.setTitle(isEditMode ? "Edit Affine Set" : "Create Affine Set");

    VBox dialogVbox = new VBox(10);
    dialogVbox.setPadding(new Insets(10));

    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(10, 150, 10, 10));

    TextField stepsField = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentSteps()) : "");
    grid.add(new Label("Number of Steps:"), 0, 0);
    grid.add(stepsField, 1, 0);

    TextField minCoordsFieldX = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMinCoords().getX0()) : "");
    TextField minCoordsFieldY = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMinCoords().getX1()) : "");
    grid.add(new Label("Min Coordinates X:"), 0, 1);
    grid.add(minCoordsFieldX, 1, 1);
    grid.add(new Label("Min Coordinates Y:"), 2, 1);
    grid.add(minCoordsFieldY, 3, 1);

    TextField maxCoordsFieldX = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMaxCoords().getX0()) : "");
    TextField maxCoordsFieldY = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMaxCoords().getX1()) : "");
    grid.add(new Label("Max Coordinates X:"), 0, 2);
    grid.add(maxCoordsFieldX, 1, 2);
    grid.add(new Label("Max Coordinates Y:"), 2, 2);
    grid.add(maxCoordsFieldY, 3, 2);

    AtomicInteger rowIndex = new AtomicInteger(3);
    grid.add(new Label("Matrix"), 0, rowIndex.get());
    grid.add(new Label("Vector"), 1, rowIndex.get());
    rowIndex.incrementAndGet();

    List<Node[]> transformationFields = new ArrayList<>();

    if (isEditMode) {
      List<Transform2D> affineTransformations = gameController.getAffineTransformations();
      for (Transform2D transform : affineTransformations) {
        if (transform instanceof AffineTransform2D) {
          addTransformationFields(grid, (
              AffineTransform2D) transform, rowIndex.get(), transformationFields);
          rowIndex.incrementAndGet();
        }
      }
    } else {
      for (int i = 0; i < 3; i++) {
        addEmptyTransformationFields(grid, rowIndex.get(), transformationFields);
        rowIndex.incrementAndGet();
      }
    }

    ScrollPane scrollPane = new ScrollPane(grid);
    scrollPane.setFitToWidth(true);
    scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    HBox editBox = new HBox(10);
    editBox.setAlignment(Pos.CENTER_LEFT);

    Button editButton = new Button(isEditMode ? "Edit Fractal" : "Display Fractal");
    editButton.setOnAction(event -> {
      if (validateInputs(stepsField, minCoordsFieldX,
          minCoordsFieldY, maxCoordsFieldX, maxCoordsFieldY, transformationFields)) {
        displayFractal(stepsField, minCoordsFieldX,
            minCoordsFieldY, maxCoordsFieldX, maxCoordsFieldY, transformationFields);
      }
    });

    HBox buttonBox = new HBox(10);
    buttonBox.setAlignment(Pos.CENTER_RIGHT);

    Button addBtn = new Button("Add Transformation");
    addBtn.setOnAction(event -> {
      addEmptyTransformationFields(grid, rowIndex.get(), transformationFields);
      rowIndex.incrementAndGet();
    });

    Button removeBtn = new Button("Remove Transformation");
    removeBtn.setOnAction(event -> {
      if (!transformationFields.isEmpty()) {
        Node[] lastFields = transformationFields.remove(transformationFields.size() - 1);
        grid.getChildren().removeAll(lastFields);
        rowIndex.decrementAndGet();
      }
    });

    editBox.getChildren().addAll(editButton);
    buttonBox.getChildren().addAll(addBtn, removeBtn);

    dialogVbox.getChildren().addAll(scrollPane, editBox, buttonBox);
    dialog.getDialogPane().setContent(dialogVbox);
    dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
  }

  /**
   * Displays the fractal based on the user inputs.
   *
   * @param stepsField          the text field for the number of steps
   * @param minCoordsFieldX     the text field for the minimum X coordinate
   * @param minCoordsFieldY     the text field for the minimum Y coordinate
   * @param maxCoordsFieldX     the text field for the maximum X coordinate
   * @param maxCoordsFieldY     the text field for the maximum Y coordinate
   * @param transformationFields the list of transformation fields
   */
  private void displayFractal(TextField stepsField, TextField minCoordsFieldX,
                              TextField minCoordsFieldY, TextField maxCoordsFieldX,
                              TextField maxCoordsFieldY, List<Node[]> transformationFields) {
    int steps = Integer.parseInt(stepsField.getText());
    double minX = Double.parseDouble(minCoordsFieldX.getText());
    double minY = Double.parseDouble(minCoordsFieldY.getText());
    double maxX = Double.parseDouble(maxCoordsFieldX.getText());
    double maxY = Double.parseDouble(maxCoordsFieldY.getText());

    List<Transform2D> transforms = new ArrayList<>();
    for (Node[] fields : transformationFields) {
      TextField matrixField = (TextField) fields[0];
      TextField vectorField = (TextField) fields[1];
      double[][] matrixValues = parseMatrix(matrixField.getText());
      double[] vectorValues = parseVector(vectorField.getText());
      Matrix2x2 matrix = new Matrix2x2(matrixValues[0][0], matrixValues[0][1],
          matrixValues[1][0], matrixValues[1][1]);
      Vector2D vector = new Vector2D(vectorValues[0], vectorValues[1]);
      transforms.add(new AffineTransform2D(matrix, vector));
    }
    gameController.setChaosGameSteps(steps);
    gameController.setAffineTransformation(transforms, new Vector2D(minX, minY),
        new Vector2D(maxX, maxY));
    gameController.setChaosCanvas();
    gameController.runTransformation();
    dialog.close();
    Platform.runLater(() -> {
      if (gameController.getOutOfBoundsCount() > 0) {
        UserFeedback.showErrorPopup("Error",
            gameController.getOutOfBoundsCount() + " pixels are out of bounds.");
      }
    });
  }

  /**
   * Validates the user inputs.
   *
   * @param stepsField          the text field for the number of steps
   * @param minCoordsFieldX     the text field for the minimum X coordinate
   * @param minCoordsFieldY     the text field for the minimum Y coordinate
   * @param maxCoordsFieldX     the text field for the maximum X coordinate
   * @param maxCoordsFieldY     the text field for the maximum Y coordinate
   * @param transformationFields the list of transformation fields
   * @return true if the inputs are valid, false otherwise
   */
  private boolean validateInputs(TextField stepsField, TextField minCoordsFieldX,
                                 TextField minCoordsFieldY, TextField maxCoordsFieldX,
                                 TextField maxCoordsFieldY, List<Node[]> transformationFields) {
    try {
      // Validate step count
      int steps = Integer.parseInt(stepsField.getText());
      if (steps <= 0 || steps > 1_000_000) {
        throw new IllegalArgumentException("Steps must be between 1 and 1,000,000.");
      }

      // Validate coordinate bounds
      double minX = Double.parseDouble(minCoordsFieldX.getText());
      double minY = Double.parseDouble(minCoordsFieldY.getText());
      double maxX = Double.parseDouble(maxCoordsFieldX.getText());
      double maxY = Double.parseDouble(maxCoordsFieldY.getText());
      if (minX >= maxX || minY >= maxY) {
        throw new IllegalArgumentException(
            "Minimum coordinates must be less than maximum coordinates.");
      }

      for (Node[] fields : transformationFields) {
        TextField matrixField = (TextField) fields[0];
        TextField vectorField = (TextField) fields[1];
        double[][] matrix = parseMatrix(matrixField.getText());
        double[] vector = parseVector(vectorField.getText());
        for (double[] row : matrix) {
          for (double value : row) {
            if (value < -2.0 || value > 2.0) {
              throw new IllegalArgumentException("Matrix values must be between -2 and 2.");
            }
          }
        }
        for (double value : vector) {
          if (value < -2.0 || value > 2.0) {
            throw new IllegalArgumentException("Vector values must be between -2 and 2.");
          }
        }
      }
    } catch (NumberFormatException e) {
      UserFeedback.showErrorPopup("Input Error", "Please enter valid numbers.");
      return false;
    } catch (IllegalArgumentException e) {
      UserFeedback.showErrorPopup("Input Error", e.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Adds transformation fields to the grid for an existing transformation.
   *
   * @param grid         the grid to which the fields are added
   * @param transform    the affine transformation
   * @param rowIndex     the row index in the grid
   * @param fieldList    the list of transformation fields
   */
  private void addTransformationFields(GridPane grid, AffineTransform2D transform,
                                       int rowIndex, List<Node[]> fieldList) {
    TextField matrixField = new TextField("[" + transform.getMatrix().getA00()
        + ", " + transform.getMatrix().getA01() + "; " + transform.getMatrix().getA10()
        + ", " + transform.getMatrix().getA11() + "]");
    TextField vectorField = new TextField("[" + transform.getVector().getX0()
        + ", " + transform.getVector().getX1() + "]");
    grid.add(matrixField, 0, rowIndex);
    grid.add(vectorField, 1, rowIndex);
    fieldList.add(new Node[]{matrixField, vectorField});
  }

  /**
   * Adds empty transformation fields to the grid.
   *
   * @param grid      the grid to which the fields are added
   * @param rowIndex  the row index in the grid
   * @param fieldList the list of transformation fields
   */
  private void addEmptyTransformationFields(
      GridPane grid, int rowIndex, List<Node[]> fieldList) {
    TextField matrixField = new TextField("[" + 1.0 + ", " + 0.0 + "; "
        + 0.0 + ", " + 1.0 + "]");
    TextField vectorField = new TextField("[" + 0.0 + ", " + 0.0 + "]");
    grid.add(matrixField, 0, rowIndex);
    grid.add(vectorField, 1, rowIndex);
    fieldList.add(new Node[]{matrixField, vectorField});
  }

  /**
   * Parses a matrix from the given text.
   *
   * @param text the text representing the matrix
   * @return the parsed matrix
   */
  private double[][] parseMatrix(String text) {
    text = text.replaceAll("[\\[\\]]", "");
    String[] rows = text.split(";");
    double[][] matrix = new double[2][2];
    for (int i = 0; i < rows.length; i++) {
      String[] values = rows[i].split(",");
      matrix[i][0] = Double.parseDouble(values[0].trim());
      matrix[i][1] = Double.parseDouble(values[1].trim());
    }
    return matrix;
  }

  /**
   * Parses a vector from the given text.
   *
   * @param text the text representing the vector
   * @return the parsed vector
   */
  private double[] parseVector(String text) {
    text = text.replaceAll("[\\[\\]]", "");
    String[] values = text.split(",");
    return new double[]{Double.parseDouble(values[0].trim()), Double.parseDouble(values[1].trim())};
  }

  /**
   * Shows the dialog.
   */
  public void showDialog() {
    dialog.showAndWait();
  }
}