package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

/**
 * This class represents a menu for choosing existing fractals.
 */
public class ExistingFractalsMenu extends VBox {

  /**
   * Constructor for the ExistingFractalsMenu class.
   *
   * @param gameController the game controller
   */
  public ExistingFractalsMenu(GameController gameController, FileController fileController) {
    Label headline = new Label("Choose existing fractal");
    headline.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");
    Button sierpinskiTriangleButton = new Button("Sierpinski Triangle");
    sierpinskiTriangleButton.setOnAction(event -> {
      gameController.createSierpinskiTriangle();
      gameController.setChaosGameSteps(1000000);
      gameController.setChaosCanvas();
      gameController.runTransformation();
    });
    Button barnsleyFernButton = new Button("Barnsley Fern");
    barnsleyFernButton.setOnAction(event -> {
      gameController.createBarnsleyFern();
      gameController.setChaosGameSteps(1000000);
      gameController.setChaosCanvas();
      gameController.runTransformation();
    });
    Button juliaButton = new Button("Julia Transformation");
    juliaButton.setOnAction(event -> {
      gameController.createJuliaTransformation();
      gameController.setChaosGameSteps(1000000);
      gameController.setChaosCanvas();
      gameController.runTransformation();
    });

    Button savedFractalsButton = new Button("Saved Fractals");
    savedFractalsButton.setOnAction(
        event -> showSavedFractalsDialog(gameController, fileController));

    getChildren().addAll(headline, sierpinskiTriangleButton,
        barnsleyFernButton, juliaButton, savedFractalsButton);
    setAlignment(Pos.TOP_CENTER);
    setSpacing(10);
  }

  /**
   * Shows a dialog with saved fractals.
   *
   * @param gameController the game controller
   */
  private void showSavedFractalsDialog(
      GameController gameController, FileController fileController) {
    if (fileController.listTransformationFileNames().isEmpty()) {
      UserFeedback.showErrorPopup("Error", "There are no saved fractals");
      return;
    }
    Dialog<String> dialog = new Dialog<>();
    dialog.setTitle("Select a Fractal");
    dialog.setHeaderText("Choose a fractal to display:");

    ButtonType loadButtonType = new ButtonType("Display", ButtonBar.ButtonData.OK_DONE);
    dialog.getDialogPane().getButtonTypes().addAll(loadButtonType, ButtonType.CANCEL);

    ListView<String> listView = new ListView<>();
    List<String> savedFractals = fileController.listTransformationFileNames();
    listView.setItems(FXCollections.observableArrayList(savedFractals));
    listView.setPrefHeight(180);
    dialog.getDialogPane().setContent(listView);

    dialog.setResultConverter(dialogButton -> {
      if (dialogButton == loadButtonType && !listView.getSelectionModel().isEmpty()) {
        return listView.getSelectionModel().getSelectedItem();
      }
      return null;
    });

    dialog.showAndWait().ifPresent(fractalName -> {
      try {
        fileController.readFractalFromFile(fractalName);
        gameController.setChaosGameSteps(1000000);
        gameController.setChaosCanvas();
        gameController.runTransformation();
      } catch (Exception e) {
        UserFeedback.showErrorPopup("Error", "Failed to load fractal: " + e.getMessage());
      }
    });
  }
}