package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.transform.Rotate;

/**
 * The RotationButton class represents a set of buttons for rotating a Node.
 * It provides clockwise and counterclockwise rotation functionality.
 */
public class RotationButton extends HBox {

  // Private Button object for rotating clockwise.
  private final Button rotateClockwise;

  // Private Button object for rotating counterclockwise.
  private final Button rotateCounterClockwise;

  // Private Node object.
  private final Node node;

  // Private GameController object.
  private GameController gameController;

  // Private double for total rotation.
  private double totalRotation = 0.0;

  /**
   * Constructs a new RotationButton.
   *
   * @param node           The Node to be rotated.
   * @param gameController The GameController associated with the RotationButton.
   */
  public RotationButton(Node node, GameController gameController) {
    this.node = node;
    this.gameController = gameController;
    rotateClockwise = new Button("Right");
    rotateCounterClockwise = new Button("Left");

    initializeUi();
    attachEventHandlers();
  }

  /**
   * Initializes the UI components of the RotationButton.
   */
  private void initializeUi() {
    setSpacing(10);
    setAlignment(Pos.CENTER);
    getChildren().addAll(rotateCounterClockwise, rotateClockwise);
  }

  /**
   * Attaches event handlers to the buttons.
   */
  private void attachEventHandlers() {
    rotateClockwise.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        rotate(90);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to rotate");
      }
    });
    rotateCounterClockwise.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        rotate(-90);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to rotate");
      }
    });
  }

  /**
   * Rotates the node by the specified angle.
   *
   * @param angle The angle by which to rotate the node.
   */
  private void rotate(double angle) {
    clearTransforms();
    double centerX = getCenterX();
    double centerY = getCenterY();
    double pivotX = getPivotX(centerX);
    double pivotY = getPivotY(centerY);
    totalRotation += angle;
    node.getTransforms().add(new Rotate(totalRotation, pivotX, pivotY));
  }

  /**
   * Clears all the transformations applied to the node.
   */
  private void clearTransforms() {
    node.getTransforms().clear();
  }

  /**
   * Calculates the x-coordinate of the center of the scene.
   *
   * @return The x-coordinate of the center of the scene.
   */
  private double getCenterX() {
    return node.getScene().getWidth() / 2;
  }

  /**
   * Calculates the y-coordinate of the center of the scene.
   *
   * @return The y-coordinate of the center of the scene.
   */
  private double getCenterY() {
    return node.getScene().getHeight() / 2;
  }

  /**
   * Calculates the x-coordinate of the pivot point for rotation.
   *
   * @param centerX The x-coordinate of the center of the scene.
   * @return The x-coordinate of the pivot point.
   */
  private double getPivotX(double centerX) {
    Bounds bounds = node.getBoundsInParent();
    double nodeCenterX = bounds.getWidth() / 2;
    double deltaX = centerX - node.localToScene(nodeCenterX, 0).getX();
    return nodeCenterX + deltaX;
  }

  /**
   * Calculates the y-coordinate of the pivot point for rotation.
   *
   * @param centerY The y-coordinate of the center of the scene.
   * @return The y-coordinate of the pivot point.
   */
  private double getPivotY(double centerY) {
    Bounds bounds = node.getBoundsInParent();
    double nodeCenterY = bounds.getHeight() / 2;
    double deltaY = centerY - node.localToScene(0, nodeCenterY).getY();
    return nodeCenterY + deltaY;
  }
}
