package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.utils.Size;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * This class represents the extra user options for the program.
 */
public class ExtraUserOptions extends VBox {

  // Button for resetting the view.
  private final Button resetViewButton;

  // Button for clearing the canvas.
  private final Button clearCanvasButton;

  // The node.
  private final Node node;

  // Button for rotating the view.
  private final RotationButton rotationButton;

  // Button for additional information.
  private final InformationButton informationButton;

  // Private GameController object.
  private GameController gameController;

  // Private Zoom object.
  private Zoom zoom;

  // Button for zooming in.
  private Button zoomInButton;

  // Button for zooming out.
  private Button zoomOutButton;

  // Button for moving up in the canvas.
  private Button upButton;

  // Button for moving down in the canvas.
  private Button downButton;

  // Button for moving left in the canvas.
  private Button leftButton;

  // Button for moving right in the canvas.
  private Button rightButton;

  /**
   * Constructor for the ExtraUserOptions class.
   *
   * @param node the node
   * @param gameController the game controller
   */
  public ExtraUserOptions(Node node, GameController gameController) {
    this.node = node;
    this.gameController = gameController;
    resetViewButton = new Button("Reset View");
    clearCanvasButton = new Button("Clear Canvas");
    rotationButton = new RotationButton(node, gameController);
    informationButton = new InformationButton("Additional Information");
    this.zoom = new Zoom(gameController);
    this.zoomOutButton = new Button("-");
    this.zoomInButton = new Button("+");
    this.upButton = new Button("Up");
    this.downButton = new Button("Down");
    this.leftButton = new Button("Left");
    this.rightButton = new Button("Right");
    getStylesheets().add(getClass().getResource(
        "/edu/ntnu/idatt2003/mappevurderingprog2/styles/extraUserOptions.css").toExternalForm());
    this.getStyleClass().add("extra-user-options");

    setupZoomButtons();
    extraUserOptionsLabel();

    Label rotationLabel = new Label("Rotate the view");
    rotationLabel.getStyleClass().add("rotation-label");

    Label zoomLabel = new Label("Zoom");
    zoomLabel.getStyleClass().add("zoom-label");

    Label navigateLabel = new Label("Navigate");
    navigateLabel.getStyleClass().add("navigate-label");

    HBox zoomControls = new HBox(zoomOutButton, zoomInButton);
    zoomControls.setAlignment(Pos.CENTER);
    zoomControls.setSpacing(10);

    HBox leftRightControls = new HBox(leftButton, rightButton);
    leftRightControls.setAlignment(Pos.CENTER);
    leftRightControls.setSpacing(10);

    this.getChildren().addAll(
        zoomLabel,
        zoomControls,
        navigateLabel,
        upButton,
        leftRightControls,
        downButton,
        rotationLabel,
        rotationButton,
        createSeparator(),
        resetViewButton,
        clearCanvasButton,
        informationButton
    );
    extraUserOptionsInitialize();
  }

  /**
   * Sets up the actions for the zoom buttons.
   * Checks if the fractal exists before performing the zoom actions.
   */
  private void setupZoomButtons() {
    zoomInButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        zoom.zoomIn();
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to zoom in on");
      }
    });
    zoomOutButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        zoom.zoomOut();
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to zoom out on");
      }
    });
    upButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        zoom.moveUp(0.03);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to move up");
      }
    });
    downButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        zoom.moveDown(0.03);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to move down");
      }
    });
    leftButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        zoom.moveLeft(0.03);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to move left");
      }
    });
    rightButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        zoom.moveRight(0.03);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to move right");
      }
    });
  }

  /**
   * Initializes additional settings for the extra user options.
   * Sets sizes, styles, and actions for reset and clear buttons.
   */
  private void extraUserOptionsInitialize() {
    extraUserOptionsSizeAndPositioning();
    extraUserOptionsStyling();
    resetZoomButton();
    clearCanvasButton();
  }

  /**
   * Sets up the action for the reset view button.
   * Resets the fractal view and canvas coordinates.
   */
  private void resetZoomButton() {
    resetViewButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        gameController.resetChaosCanvasCoordinates();
        gameController.setChaosCanvas();
        gameController.runTransformation();
        node.getTransforms().clear();
        node.setTranslateX(0);
        node.setTranslateY(0);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to reset the view for");
      }
    });
  }

  /**
   * This method sets the size and positioning of the extra user options.
   */
  private void extraUserOptionsSizeAndPositioning() {
    double screenWidth = Size.getScreenWidth();
    double screenHeight = Size.getScreenHeight();
    setPrefWidth(0.2 * screenWidth);
    setMaxWidth(0.2 * screenWidth);
    setPrefHeight(0.3 * screenHeight);
    setMaxHeight(0.3 * screenHeight);
    setAlignment(Pos.CENTER);
    setPadding(new Insets(10, 20, 10, 20));
    setSpacing(10);
  }

  /**
   * Applies styling properties to the extra user options panel.
   */
  private void extraUserOptionsStyling() {
    this.getStyleClass().add("extra-user-options");
  }

  /**
   * Sets up the action for the clear canvas button.
   * Clears the fractal from the canvas and resets the node transformations.
   */
  private void clearCanvasButton() {
    clearCanvasButton.setOnAction(event -> {
      if (!gameController.isChaosGameEmpty()) {
        if (node instanceof Canvas) {
          GraphicsContext gc = ((Canvas) node).getGraphicsContext2D();
          gc.clearRect(0, 0, ((Canvas) node).getWidth(), ((Canvas) node).getHeight());
        }
        gameController.emptyChaosGame();
        node.getTransforms().clear();
        node.setTranslateX(0);
        node.setTranslateY(0);
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to clear");
      }
    });
  }

  /**
   * Adds a label for the extra user options section.
   */
  private void extraUserOptionsLabel() {
    Label extraUserOptionsLabel = new Label("Extra Functions");
    extraUserOptionsLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");
    getChildren().add(extraUserOptionsLabel);
  }

  /**
   * Creates a separator for the extra user options panel.
   *
   * @return the separator
   */
  private Separator createSeparator() {
    Separator separator = new Separator();
    separator.setMaxWidth(Double.MAX_VALUE);
    VBox.setVgrow(separator, Priority.ALWAYS);
    return separator;
  }
}
