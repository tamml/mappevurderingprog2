package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.CanvasController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.views.View;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.VBox;

/**
 * The Menu class represents the main menu of the application.
 * It provides options for managing existing fractals, creating new fractals,
 * editing the current fractal, scaling the canvas size, and quitting the application.
 */
public class Menu extends VBox {

  // Private ExistingFractalsMenu object.
  private ExistingFractalsMenu existingFractalsMenu;

  // Private CreateFractalMenu object.
  private CreateFractalMenu createFractalMenu;

  // Private CurrentFractalMenu object.
  private CurrentFractalMenu currentFractalMenu;

  // Private ScaleCanvasSize object.
  private ScaleCanvasSize scaleCanvasSize;

  // Private quit Button object.
  private Button quitButton;

  /**
   * Constructor for the Menu class.
   *
   * @param view the view
   * @param gameController the game controller
   * @param canvasController the canvas controller
   */
  public Menu(View view, GameController gameController, CanvasController canvasController,
              FileController fileController) {
    existingFractalsMenu = new ExistingFractalsMenu(gameController, fileController);
    createFractalMenu = new CreateFractalMenu(gameController);
    currentFractalMenu = new CurrentFractalMenu(gameController, fileController);
    scaleCanvasSize = new ScaleCanvasSize(view, gameController, canvasController);
    quitButton = new Button("Quit application");
    initializeMenu();
  }

  /**
   * Initializes the menu by setting up its components and layout.
   */
  private void initializeMenu() {
    getStylesheets().add(getClass().getResource(
        "/edu/ntnu/idatt2003/mappevurderingprog2/styles/menu.css").toExternalForm());
    Label menuLabel = new Label("Menu");
    menuLabel.getStyleClass().add("menu-label");
    quitButton.getStyleClass().add("quit-button");
    quitButton.setOnAction(e -> Platform.exit());

    VBox.setMargin(menuLabel, new Insets(7, 0, 7, 0));
    VBox.setMargin(existingFractalsMenu, new Insets(7, 0, 7, 0));
    VBox.setMargin(createFractalMenu, new Insets(2, 0, 2, 0));
    VBox.setMargin(currentFractalMenu, new Insets(2, 0, 2, 0));
    VBox.setMargin(quitButton, new Insets(5, 0, 50, 0));


    getChildren().addAll(
        new Separator(),
        menuLabel,
        new Separator(),
        existingFractalsMenu,
        createFractalMenu,
        currentFractalMenu,
        scaleCanvasSize,
        new Separator(),
        quitButton,
        new Separator());
    setAlignment(Pos.TOP_CENTER);
    setSpacing(20);
    this.getStyleClass().add("menu-box");
  }
}