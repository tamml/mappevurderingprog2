package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;

/**
 * This class represents a button for showing additional information.
 */
public class InformationButton extends Button {

  /**
   * Constructor for the InformationButton class.
   *
   * @param label the label
   */
  public InformationButton(String label) {
    super(label);

    this.setOnAction(event -> {
      Alert alert = new Alert(AlertType.INFORMATION);
      alert.setTitle("Additional information");
      alert.setHeaderText(null);

      String contentText = "Fractals use 3 primary colors to show hit density:\n"
          + "Red: Top 33% hit most\nBlue: Mid 33%\nGreen: Bottom 33%\n\n"
          + "Left Side: Main menu - create, select, edit fractals\n"
          + "Right Side: Extra user options for canvas interaction\n"
          + "The reset view button brings the canvas back to its original zoom and rotation\n"
          + "The clear canvas button clears the whole canvas\n\n"
          + "Interact with canvas: use zoom buttons + to zoom in and the - to zoom out, \n "
          + "Move around the canvas with navigate buttons.";
      alert.setContentText(contentText);

      alert.getDialogPane().setPrefWidth(400);
      alert.getDialogPane().setPrefHeight(300);

      alert.getDialogPane().setMinWidth(Region.USE_PREF_SIZE);
      alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);

      alert.getButtonTypes().setAll(ButtonType.CLOSE);

      alert.showAndWait();
    });
  }
}
