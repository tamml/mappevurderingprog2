package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import java.util.Optional;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This class represents a menu for the current fractal.
 */
public class CurrentFractalMenu extends VBox {

  /**
   * Constructor for the CurrentFractalMenu class.
   *
   * @param gameController the game controller
   * @param fileController the file controller
   */
  public CurrentFractalMenu(GameController gameController, FileController fileController) {
    Label editLabel = new Label("Current fractal");
    editLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

    Button editButton = new Button("Edit");
    editButton.setOnAction(event -> {
      if (gameController.isChaosGameEmpty()) {
        UserFeedback.showErrorPopup("Error", "There is no fractal to edit");
      } else if (gameController.isJuliaTransformation()) {
        JuliaDialog juliaDialog = new JuliaDialog(gameController, true);
        juliaDialog.showDialog();
      } else if (gameController.isAffineTransformation()) {
        AffineDialog affineDialog = new AffineDialog(gameController, true);
        affineDialog.showDialog();
      }
    });

    Button saveButton = new Button("Save");
    saveButton.setOnAction(event -> {
      if (gameController.isChaosGameEmpty()) {
        UserFeedback.showErrorPopup("Error", "There is no fractal to save");
      } else {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Save fractal");
        dialog.setHeaderText("Enter a name for the fractal");
        dialog.setContentText("Name:");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(name -> {
          if (fileController.doesFileExist(name)) {
            UserFeedback.showErrorPopup("Error", "A fractal with that name already exists");
          } else {
            try {
              fileController.saveFractalToFile(name);
              UserFeedback.showConfirmationPopup("Success", "Fractal saved successfully");
            } catch (Exception e) {
              throw new RuntimeException(e);
            }
          }
        });
      }
    });

    HBox buttonBox = new HBox(editButton, saveButton);
    buttonBox.setAlignment(Pos.CENTER);
    buttonBox.setSpacing(10);

    getChildren().addAll(editLabel, buttonBox);
    setAlignment(Pos.TOP_CENTER);
    setSpacing(10);
  }
}
