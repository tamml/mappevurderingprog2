package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import javafx.scene.control.Alert;

/**
 * The UserFeedback class provides methods
 * for displaying different types of popup messages.
 */
public class UserFeedback {

  /**
   * Shows a confirmation popup with the given title and message.
   *
   * @param title   The title of the popup.
   * @param message The message of the popup.
   */
  public static void showConfirmationPopup(String title, String message) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }

  /**
   * Displays an error popup message.
   *
   * @param title   The title of the popup.
   * @param message The message to be displayed.
   */
  public static void showErrorPopup(String title, String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }
}
