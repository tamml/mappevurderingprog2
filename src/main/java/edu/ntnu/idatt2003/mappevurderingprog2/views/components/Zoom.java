package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;

/**
 * The Zoom class provides methods for zooming in,
 * zooming out, and moving the view in a canvas.
 */
public class Zoom {

  // Private GameController object.
  private GameController gameController;

  /**
   * Constructs a new Zoom object.
   *
   * @param gameController The GameController object associated with the Zoom object.
   */
  public Zoom(GameController gameController) {
    this.gameController = gameController;
  }

  /**
   * Zooms in on the canvas.
   */
  public void zoomIn() {
    adjustZoom(0.9);
  }

  /**
   * Zooms out on the canvas.
   */
  public void zoomOut() {
    adjustZoom(1.1);
  }

  /**
   * Moves the view to the left in the canvas.
   *
   * @param amount The amount to move the view.
   */
  public void moveLeft(double amount) {
    translateView(-amount, 0);
  }

  /**
   * Moves the view to the right in the canvas.
   *
   * @param amount The amount to move the view.
   */
  public void moveRight(double amount) {
    translateView(amount, 0);
  }

  /**
   * Moves the view up in the canvas.
   *
   * @param amount The amount to move the view.
   */
  public void moveUp(double amount) {
    translateView(0, amount);
  }

  /**
   * Moves the view down in the canvas.
   *
   * @param amount The amount to move the view.
   */
  public void moveDown(double amount) {
    translateView(0, -amount);
  }

  /**
   * Adjusts the zoom level of the canvas.
   *
   * @param adjustFactor The factor by which to adjust the zoom level.
   */
  private void adjustZoom(double adjustFactor) {
    Vector2D currentMin = gameController.getCurrentMinCoords();
    Vector2D currentMax = gameController.getCurrentMaxCoords();

    double centerX = (currentMax.getX0() + currentMin.getX0()) / 2;
    double centerY = (currentMax.getX1() + currentMin.getX1()) / 2;

    double newMinX = centerX + (currentMin.getX0() - centerX) * adjustFactor;
    double newMaxX = centerX + (currentMax.getX0() - centerX) * adjustFactor;
    double newMinY = centerY + (currentMin.getX1() - centerY) * adjustFactor;
    double newMaxY = centerY + (currentMax.getX1() - centerY) * adjustFactor;

    updateCoordinates(newMinX, newMaxX, newMinY, newMaxY);
  }

  /**
   * Translates the view by the specified amount.
   *
   * @param deltaX The amount to move along the x-axis.
   * @param deltaY The amount to move along the y-axis.
   */
  private void translateView(double deltaX, double deltaY) {
    Vector2D currentMin = gameController.getCurrentMinCoords();
    Vector2D currentMax = gameController.getCurrentMaxCoords();

    double newMinX = currentMin.getX0() + deltaX;
    double newMaxX = currentMax.getX0() + deltaX;
    double newMinY = currentMin.getX1() + deltaY;
    double newMaxY = currentMax.getX1() + deltaY;

    updateCoordinates(newMinX, newMaxX, newMinY, newMaxY);
  }

  /**
   * Updates the coordinates of the canvas view.
   *
   * @param newMinX The new minimum x-coordinate.
   * @param newMaxX The new maximum x-coordinate.
   * @param newMinY The new minimum y-coordinate.
   * @param newMaxY The new maximum y-coordinate.
   */
  private void updateCoordinates(double newMinX, double newMaxX, double newMinY, double newMaxY) {
    Vector2D newMin = new Vector2D(newMinX, newMinY);
    Vector2D newMax = new Vector2D(newMaxX, newMaxY);

    gameController.updateChaosCanvasCoordinates(newMin, newMax);
    gameController.setChaosCanvas();
    gameController.runTransformation();
  }
}
