package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.CanvasController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.views.View;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * The ScaleCanvasSize class represents a component for scaling the canvas size.
 * It provides options to increase or decrease the height and width of the canvas.
 */
public class ScaleCanvasSize extends VBox {

  // Constants for minimum width.
  private static final double MIN_WIDTH = 100;

  // Constants for minimum height.
  private static final double MIN_HEIGHT = 100;

  // Constants for maximum width.
  private static final double MAX_WIDTH = 800;

  // Constants for maximum height.
  private static final double MAX_HEIGHT = 700;

  // Private GameController object.
  private GameController gameController;

  // Private CanvasController object.
  private CanvasController canvasController;

  /**
   * Constructs a new ScaleCanvasSize component.
   *
   * @param view           The View object associated with the canvas.
   * @param gameController The GameController object associated with the canvas.
   */
  public ScaleCanvasSize(View view, GameController gameController,
                         CanvasController canvasController) {
    this.gameController = gameController;
    this.canvasController = canvasController;
    Label scaleLabel = new Label("Scale canvas size");
    scaleLabel.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");

    Label heightLabel = new Label("Height");
    heightLabel.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
    Button heightIncrease = new Button("+");
    Button heightDecrease = new Button("-");
    HBox heightControls = new HBox(heightDecrease, heightIncrease);
    heightControls.setAlignment(Pos.CENTER);
    heightControls.setSpacing(10);

    heightIncrease.setOnAction(e -> {
      if (!gameController.isChaosGameEmpty()) {
        double currentHeight = view.getMainCanvas().getHeight();
        if (currentHeight + 10 <= MAX_HEIGHT) {
          canvasController.setCanvasHeight(view.getMainCanvas(), currentHeight + 10);
        }
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to increase the height of");
      }
    });

    heightDecrease.setOnAction(e -> {
      if (!gameController.isChaosGameEmpty()) {
        double currentHeight = view.getMainCanvas().getHeight();
        if (currentHeight - 10 >= MIN_HEIGHT) {
          canvasController.setCanvasHeight(view.getMainCanvas(), currentHeight - 10);
        }
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to decrease the height of");
      }
    });

    Label widthLabel = new Label("Width");
    widthLabel.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
    Button widthIncrease = new Button("+");
    Button widthDecrease = new Button("-");
    HBox widthControls = new HBox(widthDecrease, widthIncrease);
    widthControls.setAlignment(Pos.CENTER);
    widthControls.setSpacing(10);

    widthIncrease.setOnAction(e -> {
      if (!gameController.isChaosGameEmpty()) {
        double currentWidth = view.getMainCanvas().getWidth();
        if (currentWidth + 10 <= MAX_WIDTH) {
          canvasController.setCanvasWidth(view.getMainCanvas(), currentWidth + 10);
        }
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to increase the width of");
      }
    });

    widthDecrease.setOnAction(e -> {
      if (!gameController.isChaosGameEmpty()) {
        double currentWidth = view.getMainCanvas().getWidth();
        if (currentWidth - 10 >= MIN_WIDTH) {
          canvasController.setCanvasWidth(view.getMainCanvas(), currentWidth - 10);
        }
      } else {
        UserFeedback.showErrorPopup("Error", "There is no fractal to decrease the width of");
      }
    });

    getChildren().addAll(scaleLabel, heightLabel, heightControls, widthLabel, widthControls);
    setAlignment(Pos.TOP_CENTER);
    setSpacing(10);
  }
}
