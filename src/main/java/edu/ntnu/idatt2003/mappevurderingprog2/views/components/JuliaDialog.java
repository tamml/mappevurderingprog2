package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * This class represents a dialog for creating or editing a Julia set.
 */
public class JuliaDialog {

  // Private Dialog object.
  private Dialog<Void> dialog;

  // Private GameController object.
  private GameController gameController;

  // Private boolean for checking if the dialog is in edit mode.
  private boolean isEditMode;

  /**
   * Constructor for the JuliaDialog class.
   *
   * @param gameController the game controller
   * @param isEditMode     the boolean for checking if the dialog is in edit mode
   */
  public JuliaDialog(GameController gameController, boolean isEditMode) {
    this.gameController = gameController;
    this.isEditMode = isEditMode;
    setupDialog();
  }

  /**
   * Sets up the dialog window with appropriate fields and buttons.
   */
  private void setupDialog() {
    dialog = new Dialog<>();
    dialog.setTitle(isEditMode ? "Edit Julia Set" : "Create Julia Set");

    GridPane grid = new GridPane();
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(20, 150, 10, 10));

    TextField stepsField = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentSteps()) : "");
    grid.add(new Label("Number of Steps:"), 0, 0);
    grid.add(stepsField, 1, 0);

    TextField minCoordsFieldX = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMinCoords().getX0()) : "");
    TextField minCoordsFieldY = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMinCoords().getX1()) : "");
    grid.add(new Label("Min Coordinates X:"), 0, 1);
    grid.add(minCoordsFieldX, 1, 1);
    grid.add(new Label("Min Coordinates Y:"), 2, 1);
    grid.add(minCoordsFieldY, 3, 1);

    TextField maxCoordsFieldX = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMaxCoords().getX0()) : "");
    TextField maxCoordsFieldY = new TextField(
        isEditMode ? String.valueOf(gameController.getCurrentMaxCoords().getX1()) : "");
    grid.add(new Label("Max Coordinates X:"), 0, 2);
    grid.add(maxCoordsFieldX, 1, 2);
    grid.add(new Label("Max Coordinates Y:"), 2, 2);
    grid.add(maxCoordsFieldY, 3, 2);

    TextField realField = new TextField(
        isEditMode && gameController.getCurrentJuliaPoint() != null ? String.valueOf(
            gameController.getCurrentJuliaPoint().getRealPart()) : "");
    grid.add(new Label("Real Part:"), 0, 3);
    grid.add(realField, 1, 3);

    TextField imagField = new TextField(
        isEditMode && gameController.getCurrentJuliaPoint() != null ? String.valueOf(
            gameController.getCurrentJuliaPoint().getImaginaryPart()) : "");
    grid.add(new Label("Imaginary Part:"), 0, 4);
    grid.add(imagField, 1, 4);

    Button editButton = new Button("Display Fractal");
    editButton.setOnAction(event -> {
      if (validateInputs(stepsField, minCoordsFieldX, minCoordsFieldY,
          maxCoordsFieldX, maxCoordsFieldY, realField, imagField)) {
        displayFractal(stepsField, minCoordsFieldX, minCoordsFieldY,
            maxCoordsFieldX, maxCoordsFieldY, realField, imagField);
      }
    });
    grid.add(editButton, 1, 5);

    dialog.getDialogPane().setContent(grid);
    dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
  }

  /**
   * Method for displaying the fractal.
   *
   * @param stepsField      the TextField for the number of steps
   * @param minCoordsFieldX the TextField for the minimum x-coordinate
   * @param minCoordsFieldY the TextField for the minimum y-coordinate
   * @param maxCoordsFieldX the TextField for the maximum x-coordinate
   * @param maxCoordsFieldY the TextField for the maximum y-coordinate
   * @param realField       the TextField for the real part
   * @param imagField       the TextField for the imaginary part
   */
  private void displayFractal(TextField stepsField, TextField minCoordsFieldX,
                              TextField minCoordsFieldY, TextField maxCoordsFieldX,
                              TextField maxCoordsFieldY, TextField realField, TextField imagField) {
    int steps = Integer.parseInt(stepsField.getText());
    double minX = Double.parseDouble(minCoordsFieldX.getText());
    double minY = Double.parseDouble(minCoordsFieldY.getText());
    double maxX = Double.parseDouble(maxCoordsFieldX.getText());
    double maxY = Double.parseDouble(maxCoordsFieldY.getText());
    double real = Double.parseDouble(realField.getText());
    double imag = Double.parseDouble(imagField.getText());

    Complex newPoint = new Complex(real, imag);

    gameController.setChaosGameSteps(steps);
    gameController.setJuliaTransformation(newPoint, new Vector2D(minX, minY),
        new Vector2D(maxX, maxY));
    gameController.setChaosCanvas();
    gameController.runTransformation();
    dialog.close();
    Platform.runLater(() -> {
      if (gameController.getOutOfBoundsCount() > 0) {
        UserFeedback.showErrorPopup("Error",
            gameController.getOutOfBoundsCount() + " pixels are out of bounds.");
      }
    });
  }

  /**
   * Validates the user inputs.
   *
   * @param stepsField the field for entering the number of steps
   * @param minFieldX  the field for entering the minimum X coordinate
   * @param minFieldY  the field for entering the minimum Y coordinate
   * @param maxFieldX  the field for entering the maximum X coordinate
   * @param maxFieldY  the field for entering the maximum Y coordinate
   * @param realField  the field for entering the real part of the Julia point
   * @param imagField  the field for entering the imaginary part of the Julia point
   * @return true if the inputs are valid, false otherwise
   */
  private boolean validateInputs(TextField stepsField, TextField minFieldX,
                                 TextField minFieldY, TextField maxFieldX,
                                 TextField maxFieldY, TextField realField,
                                 TextField imagField) {
    try {
      if (stepsField.getText().isEmpty() || minFieldX.getText().isEmpty()
          || minFieldY.getText().isEmpty() || maxFieldX.getText().isEmpty()
          || maxFieldY.getText().isEmpty() || realField.getText().isEmpty()
          || imagField.getText().isEmpty()) {
        throw new IllegalArgumentException(
            "All fields must be filled out.");
      }

      int steps = Integer.parseInt(stepsField.getText());
      if (steps <= 0 || steps > 1_000_000) {
        throw new IllegalArgumentException(
            "Steps must be between 1 and 1,000,000.");
      }

      double minX = Double.parseDouble(minFieldX.getText());
      double minY = Double.parseDouble(minFieldY.getText());
      double maxX = Double.parseDouble(maxFieldX.getText());
      double maxY = Double.parseDouble(maxFieldY.getText());
      if (minX >= maxX || minY >= maxY) {
        throw new IllegalArgumentException(
            "Minimum coordinates must be less than maximum coordinates.");
      }

      double real = Double.parseDouble(realField.getText());
      double imag = Double.parseDouble(imagField.getText());
      if (real < -2 || real > 2 || imag < -2 || imag > 2) {
        throw new IllegalArgumentException(
            "Real and imaginary parts must be within [-2, 2].");
      }
    } catch (NumberFormatException e) {
      UserFeedback.showErrorPopup(
          "Input Error", "Please enter valid numbers.");
      return false;
    } catch (IllegalArgumentException e) {
      UserFeedback.showErrorPopup(
          "Input Error", e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * Method for showing the dialog.
   */
  public void showDialog() {
    dialog.showAndWait();
  }
}
