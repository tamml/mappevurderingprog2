package edu.ntnu.idatt2003.mappevurderingprog2.views.components;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This class represents a menu for creating a new fractal.
 */
public class CreateFractalMenu extends VBox {

  /**
   * Constructor for the CreateFractalMenu class.
   *
   * @param gameController the game controller
   */
  public CreateFractalMenu(GameController gameController) {
    Label headline = new Label("Create a new fractal");
    headline.setStyle("-fx-font-size: 16px; -fx-font-weight: bold;");
    Button affineButton = new Button("Affine");
    affineButton.setOnAction(event -> {
      AffineDialog affineDialog = new AffineDialog(gameController, false);
      affineDialog.showDialog();
    });

    Button juliaButton = new Button("Julia");
    juliaButton.setOnAction(event -> {
      JuliaDialog juliaDialog = new JuliaDialog(gameController, false);
      juliaDialog.showDialog();
    });

    HBox buttonBox = new HBox(affineButton, juliaButton);
    buttonBox.setAlignment(Pos.CENTER);
    buttonBox.setSpacing(10);

    getChildren().addAll(headline, buttonBox);
    setAlignment(Pos.TOP_CENTER);
    setSpacing(10);
  }
}
