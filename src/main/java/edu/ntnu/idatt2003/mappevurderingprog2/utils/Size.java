package edu.ntnu.idatt2003.mappevurderingprog2.utils;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

/**
 * A class that provides the screen size.
 */
public class Size {

  /**
   * Get the screen width.
   *
   * @return the screen width
   */
  public static double getScreenWidth() {
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    return primaryScreenBounds.getWidth();
  }

  /**
   * Get the screen height.
   *
   * @return the screen height
   */
  public static double getScreenHeight() {
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    return primaryScreenBounds.getHeight();

  }
}