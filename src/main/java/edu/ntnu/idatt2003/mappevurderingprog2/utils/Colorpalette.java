package edu.ntnu.idatt2003.mappevurderingprog2.utils;

import javafx.scene.paint.Color;

/**
 * This class represents a color palette with different colors.
 */
public final class Colorpalette {

  // The primary color for the program
  public static final Color Primary = Color.web("#5072A7");
}