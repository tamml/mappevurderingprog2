package edu.ntnu.idatt2003.mappevurderingprog2.controllers;

import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGame;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The CanvasController class is a controller class that handles the logic for the Canvas.
 * The class is responsible for updating the canvas display and setting the canvas width and height.
 */

public class CanvasController {

  // Private integer for the green threshold.
  private int greenThreshold;

  // Private integer for the blue threshold.
  private int blueThreshold;

  /**
   * Calculates the quantiles for the hits.
   */
  public void calculateQuantiles() {
    List<Integer> hitCounts = new ArrayList<>();
    for (int[] row : ChaosGame.getInstance().getCanvas().getCanvasArray()) {
      for (int hit : row) {
        if (hit > 0) {
          hitCounts.add(hit);
        }
      }
    }

    if (hitCounts.isEmpty()) {
      return;
    }

    Collections.sort(hitCounts);
    int n = hitCounts.size();
    greenThreshold = hitCounts.get((int) (n * 0.33));
    blueThreshold = hitCounts.get((int) (n * 0.66));
  }

  /**
   * Returns the color for the hits.
   *
   * @param hits the number of hits.
   * @return the color for the hits.
   */
  private Color getColorForHits(int hits) {
    if (hits == 0) {
      return Color.WHITE;
    } else if (hits <= greenThreshold) {
      return Color.GREEN;
    } else if (hits <= blueThreshold) {
      return Color.BLUE;
    } else {
      return Color.RED;
    }
  }

  /**
   * Updates the canvas display.
   *
   * @param mainCanvas the main canvas.
   * @return the updated canvas.
   */
  public Canvas updateCanvasDisplay(Canvas mainCanvas) {
    GraphicsContext gc = mainCanvas.getGraphicsContext2D();
    int[][] canvasArray = ChaosGame.getInstance().getCanvas().getCanvasArray();
    double canvasWidth = mainCanvas.getWidth() - 20;
    double canvasHeight = mainCanvas.getHeight() - 20;
    double pixelWidth = canvasWidth / canvasArray[0].length;
    double pixelHeight = canvasHeight / canvasArray.length;

    gc.clearRect(0, 0, mainCanvas.getWidth(), mainCanvas.getHeight());
    for (int i = 0; i < canvasArray.length; i++) {
      for (int j = 0; j < canvasArray[i].length; j++) {
        double x = j * pixelWidth + 3;
        double y = i * pixelHeight + 3;
        gc.setFill(getColorForHits(canvasArray[i][j]));
        gc.fillRect(x, y, pixelWidth, pixelHeight);
      }
    }
    return mainCanvas;
  }

  /**
   * Sets the canvas width.
   *
   * @param canvas the canvas.
   * @param width  the width.
   */
  public void setCanvasWidth(Canvas canvas, double width) {
    canvas.setWidth(width);
    updateCanvasDisplay(canvas);
  }

  /**
   * Sets the canvas height.
   *
   * @param canvas the canvas.
   * @param height the height.
   */
  public void setCanvasHeight(Canvas canvas, double height) {
    canvas.setHeight(height);
    updateCanvasDisplay(canvas);
  }
}
