package edu.ntnu.idatt2003.mappevurderingprog2.controllers;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.JuliaTransform;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGame;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescriptionFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * The GameController class is a controller class that handles the logic for the ChaosGame.
 * The class is responsible for creating and updating the ChaosGameDescription,
 * and running the ChaosGame.
 * The class also handles saving and reading ChaosGameDescriptions to and from files.
 */
public class GameController {

  /**
   * Creates a Sierpinski triangle ChaosGameDescription
   * and sets it as the current ChaosGameDescription.
   */
  public void createSierpinskiTriangle() {
    ChaosGame.getInstance().setDescription(ChaosGameDescriptionFactory.createSierpinskiTriangle());
  }

  /**
   * Creates a Barnsley Fern ChaosGameDescription and sets it as the current ChaosGameDescription.
   */
  public void createBarnsleyFern() {
    ChaosGame.getInstance().setDescription(ChaosGameDescriptionFactory.createBarnsleyFern());
  }

  /**
   * Creates a standard Julia transformation ChaosGameDescription
   * and sets it as the current ChaosGameDescription.
   */
  public void createJuliaTransformation() {
    ChaosGame.getInstance().setDescription(
        ChaosGameDescriptionFactory.createStandardJuliaTransformation());
  }

  /**
   * Updates the canvas coordinates of the current ChaosGameDescription.
   *
   * @param minCoords the minimum coordinates of the canvas
   * @param maxCoords the maximum coordinates of the canvas
   */
  public void updateChaosCanvasCoordinates(Vector2D minCoords, Vector2D maxCoords) {
    ChaosGameDescription description = ChaosGame.getInstance().getDescription();
    description.setMaxCoords(maxCoords);
    description.setMinCoords(minCoords);
    ChaosGame.getInstance().setDescription(description);
  }

  /**
   * Gets the current number of steps of the ChaosGame.
   *
   * @return the current number of steps
   */
  public int getCurrentSteps() {
    return ChaosGame.getInstance().getSteps();
  }

  /**
   * Gets the current minimum coordinates of the canvas.
   *
   * @return the current minimum coordinates
   */
  public Vector2D getCurrentMaxCoords() {
    return ChaosGame.getInstance().getDescription().getMaxCoords();
  }

  /**
   * Gets the current maximum coordinates of the canvas.
   *
   * @return the current maximum coordinates
   */
  public Vector2D getCurrentMinCoords() {
    return ChaosGame.getInstance().getDescription().getMinCoords();
  }

  /**
   * Gets the current Julia point of the ChaosGame.
   *
   * @return the current Julia point
   */
  public Complex getCurrentJuliaPoint() {
    List<Transform2D> transforms = ChaosGame.getInstance().getDescription().getTransforms();
    JuliaTransform julia = (JuliaTransform) transforms.get(0);
    return julia.getPoint();
  }

  /**
   * Gets the current affine transformations of the ChaosGame.
   *
   * @return the current affine transformations
   */
  public List<Transform2D> getAffineTransformations() {
    return ChaosGame.getInstance().getDescription().getTransforms();
  }


  /**
   * Sets the ChaosGame canvas.
   */
  public void setChaosCanvas() {
    ChaosGame.getInstance().setCanvas();
  }

  /**
   * Sets the number of steps of the ChaosGame.
   *
   * @param steps the number of steps to set
   */
  public void setChaosGameSteps(int steps) {
    ChaosGame.getInstance().setSteps(steps);
  }

  /**
   * Runs the ChaosGame transformations.
   */
  public void runTransformation() {
    ChaosGame.getInstance().runSteps();
  }

  /**
   * Gets the count of out-of-bounds occurrences.
   *
   * @return the count of out-of-bounds occurrences
   */
  public int getOutOfBoundsCount() {
    return ChaosGame.getInstance().getOutOfBoundsCount();
  }

  /**
   * Sets the Julia transformation with specified parameters.
   *
   * @param point     the Julia point to set
   * @param minCoords the minimum coordinates of the canvas
   * @param maxCoords the maximum coordinates of the canvas
   */
  public void setJuliaTransformation(Complex point, Vector2D minCoords, Vector2D maxCoords) {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new JuliaTransform(point, -1));
    transforms.add(new JuliaTransform(point, 1));
    ChaosGame.getInstance().setDescription(
        new ChaosGameDescription(transforms, minCoords, maxCoords));
  }

  /**
   * Sets the affine transformation with specified parameters.
   *
   * @param transforms the affine transformations to set
   * @param minCoords  the minimum coordinates of the canvas
   * @param maxCoords  the maximum coordinates of the canvas
   */
  public void setAffineTransformation(
      List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords) {
    ChaosGame.getInstance().setDescription(
        new ChaosGameDescription(transforms, minCoords, maxCoords));
  }

  /**
   * Checks if the ChaosGame is empty.
   *
   * @return true if the ChaosGame is empty, false otherwise
   */
  public boolean isChaosGameEmpty() {
    boolean empty = false;
    if (ChaosGame.getInstance().getDescription() == null) {
      empty = true;
    }
    return empty;
  }

  /**
   * Checks if the current transformation is an affine transformation.
   *
   * @return true if the current transformation is an affine transformation, false otherwise
   */
  public boolean isAffineTransformation() {
    List<Transform2D> transforms = ChaosGame.getInstance().getDescription().getTransforms();
    if (transforms.isEmpty()) {
      return false;
    }
    return transforms.get(0) instanceof AffineTransform2D;
  }

  /**
   * Checks if the current transformation is a Julia transformation.
   *
   * @return true if the current transformation is a Julia transformation, false otherwise
   */
  public boolean isJuliaTransformation() {
    List<Transform2D> transforms = ChaosGame.getInstance().getDescription().getTransforms();
    if (transforms.isEmpty()) {
      return false;
    }
    return transforms.get(0) instanceof JuliaTransform;
  }

  /**
   * Empties the ChaosGame.
   */
  public void emptyChaosGame() {
    ChaosGame.getInstance().setDescription(null);
  }

  /**
   * Resets canvas coordinates to initial values.
   */
  public void resetChaosCanvasCoordinates() {
    ChaosGameDescription description = ChaosGame.getInstance().getDescription();
    Vector2D initialMin = description.getInitialMinCoords();
    Vector2D initialMax = description.getInitialMaxCoords();

    description.setMinCoords(initialMin);
    description.setMaxCoords(initialMax);
    ChaosGame.getInstance().setDescription(description);
  }
}

