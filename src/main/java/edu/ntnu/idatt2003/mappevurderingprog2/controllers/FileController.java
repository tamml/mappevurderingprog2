package edu.ntnu.idatt2003.mappevurderingprog2.controllers;

import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGame;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGameFileHandler;
import java.util.List;

/**
 * The FileController class is a controller class that handles the logic for reading and writing
 * ChaosGame transformations to and from files.
 */
public class FileController {

  /**
   * Reads ChaosGame transformations from a file.
   *
   * @param name the name of the file to read from
   * @throws Exception if an error occurs during reading
   */
  public void saveFractalToFile(String name) throws Exception {
    ChaosGameFileHandler.writeTransformationsToFile(ChaosGame.getInstance().getDescription(), name);
  }

  /**
   * Reads ChaosGame transformations from a file.
   *
   * @param name the name of the file to read from
   * @throws Exception if an error occurs during reading
   */
  public void readFractalFromFile(String name) throws Exception {
    ChaosGameDescription description = ChaosGameFileHandler.readTransformationsFromFile(name);
    ChaosGame.getInstance().setDescription(description);
  }

  /**
   * Checks if a file with the specified name exists.
   *
   * @param name the name of the file to check
   * @return true if the file exists, false otherwise
   */
  public boolean doesFileExist(String name) {
    return ChaosGameFileHandler.checkFileExists(name);
  }

  /**
   * Lists all transformation file names.
   *
   * @return a list of transformation file names
   */
  public List<String> listTransformationFileNames() {
    return ChaosGameFileHandler.listTransformationFileNames();
  }
}
