package edu.ntnu.idatt2003.mappevurderingprog2;

import edu.ntnu.idatt2003.mappevurderingprog2.controllers.CanvasController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.FileController;
import edu.ntnu.idatt2003.mappevurderingprog2.controllers.GameController;
import edu.ntnu.idatt2003.mappevurderingprog2.models.chaos.ChaosGame;
import edu.ntnu.idatt2003.mappevurderingprog2.views.View;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The MyApp class represents the main application class.
 * It extends the Application class from JavaFX.
 */
public class MyApp extends Application {

  /**
   * Starts the application.
   *
   * @param primaryStage The primary stage of the application.
   * @throws FileNotFoundException If the file is not found.
   */
  @Override
  public void start(Stage primaryStage) throws FileNotFoundException {
    GameController gameController = new GameController();
    CanvasController canvasController = new CanvasController();
    FileController fileController = new FileController();
    View view = new View(gameController, canvasController, fileController);
    ChaosGame.getInstance().addObserver(view);
    primaryStage.setScene(view.createScene());
    primaryStage.show();
  }

  /**
   * Launches the application.
   *
   * @param args The arguments passed to the application.
   */
  public static void main(String[] args) {
    launch(args);
  }
}


