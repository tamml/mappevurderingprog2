package edu.ntnu.idatt2003.mappevurderingprog2.models.chaos;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.JuliaTransform;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * The ChaosGameFileHandler class provides static methods
 * for reading and writing chaos game descriptions
 * to and from files.
 */
public class ChaosGameFileHandler {

  /**
   * Reads a chaos game description from a file.
   *
   * @param name the name of the file containing the transformations
   * @return a ChaosGameDescription object representing the transformations read from the file
   * @throws FileNotFoundException if the specified file is not found
   */
  public static ChaosGameDescription readTransformationsFromFile(
      String name) throws FileNotFoundException {
    File file = new File(
        "src/main/resources/edu/ntnu/idatt2003/mappevurderingprog2/transformations/" + name);
    Scanner scanner = new Scanner(file).useLocale(Locale.ENGLISH);

    String transformType = scanner.nextLine().trim();

    Vector2D minCoords = parseVector2D(scanner.nextLine());
    Vector2D maxCoords = parseVector2D(scanner.nextLine());
    List<Transform2D> transforms = new ArrayList<>();

    while (scanner.hasNextLine()) {
      String line = scanner.nextLine().trim();

      if (!line.startsWith("#") && !line.isEmpty()) {
        if (transformType.equals("Affine2D")) {
          transforms.add(parseAffineTransform2D(line));
        } else if (transformType.equals("Julia")) {
          JuliaTransform transformation = parseJuliaTransform(line);
          transforms.add(transformation);
          transforms.add(new JuliaTransform(
              transformation.getPoint(), -transformation.getSign()));
        }
      }
    }
    scanner.close();

    return new ChaosGameDescription(transforms, minCoords, maxCoords);
  }

  /**
   * Writes a chaos game description to a file.
   *
   * @param description the ChaosGameDescription object to be written to the file
   * @param name        the name of the file to write the transformations to
   * @throws Exception if an error occurs while writing to the file
   */
  public static void writeTransformationsToFile(
      ChaosGameDescription description, String name) throws Exception {
    try (FileWriter writer = new FileWriter(
        "src/main/resources/edu/ntnu/idatt2003/mappevurderingprog2/transformations/" + name)) {
      if (description.getTransforms().isEmpty()) {
        return;
      }
      Transform2D firstTransform = description.getTransforms().get(0);
      if (firstTransform instanceof AffineTransform2D) {
        writer.write("Affine2D\n");
      } else if (firstTransform instanceof JuliaTransform) {
        writer.write("Julia\n");
      }

      writer.write(formatVector2D(description.getMinCoords()) + "\n");
      writer.write(formatVector2D(description.getMaxCoords()) + "\n");

      for (Transform2D transform : description.getTransforms()) {
        if (transform instanceof AffineTransform2D) {
          AffineTransform2D affine = (AffineTransform2D) transform;
          writer.write(formatMatrix2x2(affine.getMatrix())
              + ", " + formatVector2D(affine.getVector()) + "\n");
        } else if (transform instanceof JuliaTransform) {
          JuliaTransform julia = (JuliaTransform) transform;
          writer.write(formatComplex(julia.getPoint())
              + ", " + julia.getSign() + "\n");
        }
      }
    }
  }

  /**
   * Lists the filenames of transformation files available in the resources directory.
   *
   * @return a list of filenames of transformation files
   */
  public static List<String> listTransformationFileNames() {
    List<String> fileNames = new ArrayList<>();
    File directory = new File(
        "src/main/resources/edu/ntnu/idatt2003/mappevurderingprog2/transformations/");

    if (directory.exists() && directory.isDirectory()) {
      File[] files = directory.listFiles();
      if (files != null) {
        for (File file : files) {
          if (file.isFile()) {
            fileNames.add(file.getName());
          }
        }
      }
    }

    return fileNames;
  }

  /**
   * Checks if a file with the given name exists in the resources directory.
   *
   * @param name the name of the file to check for existence
   * @return true if the file exists, false otherwise
   */
  public static boolean checkFileExists(String name) {
    File file = new File(
        "src/main/resources/edu/ntnu/idatt2003/mappevurderingprog2/transformations/" + name);
    return file.exists();
  }

  /**
   * Formats a Vector2D object as a string.
   *
   * @param vector the Vector2D object to format
   * @return the formatted string representation of the Vector2D object
   */
  private static String formatVector2D(Vector2D vector) {
    return String.format(Locale.ENGLISH, "%s, %s", vector.getX0(), vector.getX1());
  }

  /**
   * Formats a Matrix2x2 object as a string.
   *
   * @param matrix the Matrix2x2 object to format
   * @return the formatted string representation of the Matrix2x2 object
   */
  private static String formatMatrix2x2(Matrix2x2 matrix) {
    return String.format(Locale.ENGLISH, "%f, %f, %f, %f",
        matrix.getA00(), matrix.getA01(),
        matrix.getA10(), matrix.getA11());
  }

  /**
   * Formats a Complex object as a string.
   *
   * @param complex the Complex object to format
   * @return the formatted string representation of the Complex object
   */
  private static String formatComplex(Complex complex) {
    return String.format(Locale.ENGLISH, "%s, %s",
        complex.getRealPart(), complex.getImaginaryPart());
  }

  /**
   * Parses a string into a Vector2D object.
   *
   * @param line the string to parse
   * @return the parsed Vector2D object
   */
  private static Vector2D parseVector2D(String line) {
    String[] parts = line.split(",");
    return new Vector2D(Double.parseDouble(parts[0].trim()),
        Double.parseDouble(parts[1].trim()));
  }

  /**
   * Parses a string into an AffineTransform2D object.
   *
   * @param line the string to parse
   * @return the parsed AffineTransform2D object
   */
  private static AffineTransform2D parseAffineTransform2D(String line) {
    String[] parts = line.split(",");
    double[][] matrixData = new double[2][2];
    matrixData[0][0] = Double.parseDouble(parts[0].trim());
    matrixData[0][1] = Double.parseDouble(parts[1].trim());
    matrixData[1][0] = Double.parseDouble(parts[2].trim());
    matrixData[1][1] = Double.parseDouble(parts[3].trim());
    Matrix2x2 matrix = new Matrix2x2(matrixData[0][0],
        matrixData[0][1], matrixData[1][0], matrixData[1][1]);

    Vector2D vector = new Vector2D(Double.parseDouble(parts[4].trim()),
        Double.parseDouble(parts[5].trim()));

    return new AffineTransform2D(matrix, vector);
  }

  /**
   * Parses a string into a JuliaTransform object.
   *
   * @param line the string to parse
   * @return the parsed JuliaTransform object
   */
  private static JuliaTransform parseJuliaTransform(String line) {
    String[] parts = line.split(",");
    Complex point = new Complex(Double.parseDouble(parts[0].trim()),
        Double.parseDouble(parts[1].trim()));
    int sign = Integer.parseInt(parts[2].trim());

    return new JuliaTransform(point, sign);
  }
}