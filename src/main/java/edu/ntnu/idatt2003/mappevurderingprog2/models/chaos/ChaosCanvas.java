package edu.ntnu.idatt2003.mappevurderingprog2.models.chaos;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;

/**
 * The ChaosCanvas class represent a 2D canvas with the ability to store and retrieve pixels.
 * The class can apply transformations to obtain pixel values from a given point.
 */
public class ChaosCanvas {

  //The canvas is represented as a 2D array of integers, where each integer represents a color.
  private int[][] canvas;

  // The width of the canvas
  private int width;

  // The height of the canvas
  private int height;

  // The minimum coordinates of the canvas
  private Vector2D minCoords;

  // The maximum coordinates of the canvas
  private Vector2D maxCoords;

  // The transformation from coordinates to indices
  private AffineTransform2D transformCoordsToIndices;

  /**
   * Constructs a ChaosCanvas object with the given
   * width, height, minimum coordinates, and maximum coordinates.
   *
   * @param width     the width of the canvas
   * @param height    the height of the canvas
   * @param minCoords the minimum coordinates of the canvas
   * @param maxCoords the maximum coordinates of the canvas
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[width][height];
    setUpTransformation();
  }

  /**
   * Sets up the transformation from coordinates to indices.
   */
  private void setUpTransformation() {
    this.transformCoordsToIndices = new AffineTransform2D(
        new Matrix2x2(0,
            ((height - 1) / (minCoords.getX1() - maxCoords.getX1())),
            ((width - 1) / (maxCoords.getX0() - minCoords.getX0())),
            0),
        new Vector2D(((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
            ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0())));
  }

  /**
   * Gets the minimum coordinates of the canvas.
   *
   * @return the minimum coordinates of the canvas
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Gets the maximum coordinates of the canvas.
   *
   * @return the maximum coordinates of the canvas
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Gets the color of the pixel at the given point.
   *
   * @param point the point at which to get the pixel color
   * @return the color of the pixel at the given point
   */
  public int getPixel(Vector2D point) {
    Vector2D indices = transformCoordsToIndices.transform(point);
    int x = (int) indices.getX0();
    int y = (int) indices.getX1();
    return canvas[x][y];
  }


  /**
   * Puts a pixel at the given point.
   *
   * @param point the point at which to put the pixel
   * @return true if the pixel was successfully put, false otherwise
   */
  public boolean putPixel(Vector2D point) {
    Vector2D indices = transformCoordsToIndices.transform(point);
    int x = (int) indices.getX0();
    int y = (int) indices.getX1();
    if (x >= 0 && x < width && y >= 0 && y < height) {
      canvas[x][y] += 1;
      return true;
    } else {
      return false;
    }
  }

  /**
   * Gets the canvas array.
   *
   * @return the canvas array
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the canvas.
   */
  public void clear() {
    for (int i = 0; i < canvas.length; i++) {
      for (int j = 0; j < canvas[i].length; j++) {
        canvas[i][j] = 0;
      }
    }
  }
}