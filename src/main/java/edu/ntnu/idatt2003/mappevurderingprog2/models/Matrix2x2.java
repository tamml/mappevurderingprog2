package edu.ntnu.idatt2003.mappevurderingprog2.models;

/**
 * This class represents a 2x2 matrix and
 * provides a method for multiplying a 2D vector with the matrix.
 * The matrix is defined by the four elements a00, a01, a10, and a11.
 */
public class Matrix2x2 {

  // The element in the first row and first column
  private double a00;

  // The element in the first row and second column
  private double a01;

  // The element in the second row and first column
  private double a10;

  // The element in the second row and second column
  private double a11;

  /**
   * Constructs a Matrix2x2 object with the specific elements.
   *
   * @param a00 the element in the first row and first column
   * @param a01 the element in the first row and second column
   * @param a10 the element in the second row and first column
   * @param a11 the element in the second row and second column
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Returns the value of the element in the first row and first column.
   *
   * @return the element in the first row and first column
   */
  public double getA00() {
    return a00;
  }

  /**
   * Returns the value of the element in the first row and second column.
   *
   * @return the element in the first row and second column
   */
  public double getA01() {
    return a01;
  }

  /**
   * Returns the value of the element in the second row and first column.
   *
   * @return the element in the second row and first column
   */
  public double getA10() {
    return a10;
  }

  /**
   * Returns the value of the element in the second row and second column.
   *
   * @return the element in the second row and second column
   */
  public double getA11() {
    return a11;
  }

  /**
   * Multiplies the given 2D vector with the matrix.
   *
   * @param vector the 2D vector to be multiplied with the matrix
   * @return a new 2D vector that is the result of the multiplication
   */
  public Vector2D multiply(Vector2D vector) {
    if (vector == null) {
      throw new IllegalArgumentException("Vector cannot be null");
    }
    return new Vector2D(a00 * vector.getX0() + a01 * vector.getX1(),
        a10 * vector.getX0() + a11 * vector.getX1());
  }
}