package edu.ntnu.idatt2003.mappevurderingprog2.models.chaos;

/**
 * The ChaosGameObserver interface provides a method to be called
 * when the chaos game is updated.
 */
public interface ChaosGameObserver {
  void onChaosGameUpdated();
}