package edu.ntnu.idatt2003.mappevurderingprog2.models.chaos;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javafx.util.Pair;

/**
 * The ChaosGameDescription class represents a description of a chaos game.
 * It contains a list of transforms, and the minimum and maximum coordinates of the game area.
 */
public class ChaosGameDescription {

  // The minimum of the x and y coordinates of the game area
  private Vector2D minCoords;

  // The maximum of the x and y coordinates of the game area
  private Vector2D maxCoords;

  // The initial minimum and maximum coordinates
  private Vector2D initialMinCoords;

  // The initial minimum and maximum coordinates
  private Vector2D initialMaxCoords;

  // The list of transforms
  private List<Transform2D> transforms;
  private List<Pair<Transform2D, Double>> weightedTransforms;
  private final boolean isWeighted;

  /**
   * Constructs a ChaosGameDescription object with the given list of transforms
   * and the minimum and maximum coordinates.
   *
   * @param transforms the list of transforms
   * @param minCoords  the minimum of the x and y coordinates of the game area
   * @param maxCoords  the maximum of the x and y coordinates of the game area
   */
  public ChaosGameDescription(
      List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
    this.isWeighted = false;
    this.initialMinCoords = new Vector2D(minCoords.getX0(), minCoords.getX1());
    this.initialMaxCoords = new Vector2D(maxCoords.getX0(), maxCoords.getX1());
  }

  /**
   * Constructs a ChaosGameDescription object with the given list of transforms
   * and the minimum and maximum coordinates.
   *
   * @param minCoords  the minimum of the x and y coordinates of the game area
   * @param maxCoords  the maximum of the x and y coordinates of the game area
   */
  public ChaosGameDescription(List<Pair<Transform2D, Double>> weightedTransforms,
                              Vector2D minCoords, Vector2D maxCoords, boolean isWeighted) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.weightedTransforms = weightedTransforms;
    this.transforms = weightedTransforms.stream().map(Pair::getKey).collect(Collectors.toList());
    this.isWeighted = isWeighted;
    this.initialMinCoords = new Vector2D(minCoords.getX0(), minCoords.getX1());
    this.initialMaxCoords = new Vector2D(maxCoords.getX0(), maxCoords.getX1());
  }

  /**
   * Gets the list of transformations associated with the chaos game.
   *
   * @return the list of transforms
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Gets the list of weighted transformations associated with the chaos game.
   *
   * @return the list of weighted transforms
   */
  public List<Pair<Transform2D, Double>> getWeightedTransforms() {
    return Collections.unmodifiableList(weightedTransforms);
  }

  /**
   * Gets the minimum of the x and y coordinates of the game area.
   *
   * @return the minimum of the x and y coordinates
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Gets the maximum of the x and y coordinates of the game area.
   *
   * @return the maximum of the x and y coordinates
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Checks if the transforms associated with the chaos game are weighted.
   *
   * @return true if the transforms are weighted, false otherwise
   */
  public boolean isWeighted() {
    return isWeighted;
  }

  /**
   * Sets the minimum coordinates of the game area.
   *
   * @param minCoords the new minimum coordinates
   * @throws IllegalArgumentException if the provided coordinates are invalid
   */
  public void setMinCoords(Vector2D minCoords) {
    if (minCoords == null) {
      throw new IllegalArgumentException("Maximum coordinates cannot be null.");
    }
    if (this.maxCoords != null && (minCoords.getX0()
        >= this.maxCoords.getX0() || minCoords.getX1() >= this.maxCoords.getX1())) {
      throw new IllegalArgumentException(
          "Minimum coordinates must be less than or equal to maximum coordinates.");
    }
    this.minCoords = minCoords;
  }

  /**
   * Sets the maximum coordinates of the game area.
   *
   * @param maxCoords the new maximum coordinates
   * @throws IllegalArgumentException if the provided coordinates are invalid
   */
  public void setMaxCoords(Vector2D maxCoords) {
    if (maxCoords == null) {
      throw new IllegalArgumentException("Maximum coordinates cannot be null.");
    }
    if (this.minCoords != null && (maxCoords.getX0()
        <= this.minCoords.getX0() || maxCoords.getX1() <= this.minCoords.getX1())) {
      throw new IllegalArgumentException(
          "Maximum coordinates must be greater than or equal to minimum coordinates.");
    }
    this.maxCoords = maxCoords;
  }

  /**
   * Gets the initial minimum coordinates of the game area.
   *
   * @return the initial minimum coordinates
   */
  public Vector2D getInitialMinCoords() {
    return initialMinCoords;
  }

  /**
   * Gets the initial maximum coordinates of the game area.
   *
   * @return the initial maximum coordinates
   */
  public Vector2D getInitialMaxCoords() {
    return initialMaxCoords;
  }

}