package edu.ntnu.idatt2003.mappevurderingprog2.models;

/**
 * This class represents a 2D vector and provides methods for vector addition and subtraction.
 * The class encapsulates the x0 and x1 components of the vector,
 * representing the x and y coordinates of the vector.
 */
public class Vector2D {

  // The component along the x-axis
  private double x0;

  // The component along the y-axis
  private double x1;

  /**
   * Constructs a Vector2D object with the given x0 and x1 components.
   *
   * @param x0 The component along the x-axis
   * @param x1 The component along the y-axis
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the value of the x0 component of the vector.
   *
   * @return the x0 component of the vector
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the value of the x1 component of the vector.
   *
   * @return the x1 component of the vector
   */
  public double getX1() {
    return x1;
  }

  /**
   * Adds another 2D vector to this vector and returns the result as a new vector.
   *
   * @param other the vector to be added to this vector
   * @return a new vector that is the result of the addition
   */
  public Vector2D add(Vector2D other) {
    if (other == null) {
      throw new IllegalArgumentException("Vector cannot be null");
    }
    return new Vector2D(x0 + other.x0, x1 + other.x1);
  }

  /**
   * Subtracts another 2D vector from this vector and returns the result as a new vector.
   *
   * @param other the vector to be subtracted from this vector
   * @return a new vector that is the result of the subtraction
   */
  public Vector2D subtract(Vector2D other) {
    if (other == null) {
      throw new IllegalArgumentException("Vector cannot be null");
    }
    return new Vector2D(x0 - other.x0, x1 - other.x1);
  }
}