package edu.ntnu.idatt2003.mappevurderingprog2.models.chaos;

import edu.ntnu.idatt2003.mappevurderingprog2.models.AffineTransform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.JuliaTransform;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Matrix2x2;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;

/**
 * The ChaosGameDescriptionFactory class provides static methods
 * to create instances of ChaosGameDescription
 * for different types of chaos games.
 */
public class ChaosGameDescriptionFactory {

  /**
   * Creates a ChaosGameDescription for generating a Sierpinski triangle.
   *
   * @return a ChaosGameDescription instance representing a Sierpinski triangle
   */
  public static ChaosGameDescription createSierpinskiTriangle() {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0)));
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)));
    return new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(1, 1));
  }

  /**
   * Creates a ChaosGameDescription for generating a Barnsley Fern.
   *
   * @return a ChaosGameDescription instance representing a Barnsley Fern
   */
  public static ChaosGameDescription createBarnsleyFern() {
    List<Pair<Transform2D, Double>> weightedTransforms = new ArrayList<>();
    weightedTransforms.add(new Pair<>(new AffineTransform2D(new Matrix2x2(
        0, 0, 0, 0.16), new Vector2D(0, 0)), 0.01));
    weightedTransforms.add(new Pair<>(new AffineTransform2D(new Matrix2x2(
        0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6)), 0.85));
    weightedTransforms.add(new Pair<>(new AffineTransform2D(new Matrix2x2(
        0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6)), 0.07));
    weightedTransforms.add(new Pair<>(new AffineTransform2D(new Matrix2x2(
        -0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44)), 0.07));
    return new ChaosGameDescription(
        weightedTransforms, new Vector2D(-3, 0), new Vector2D(3, 10), true);
  }

  /**
   * Creates a ChaosGameDescription for generating a standard Julia transformation.
   *
   * @return a ChaosGameDescription instance representing a standard Julia transformation
   */
  public static ChaosGameDescription createStandardJuliaTransformation() {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new JuliaTransform(new Complex(-0.74543, 0.11301), -1));
    transforms.add(new JuliaTransform(new Complex(-0.74543, 0.11301), 1));
    return new ChaosGameDescription(transforms, new Vector2D(-1.6, -1.0), new Vector2D(1.6, 1.0));
  }
}
