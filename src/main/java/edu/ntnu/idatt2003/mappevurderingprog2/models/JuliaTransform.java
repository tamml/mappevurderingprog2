package edu.ntnu.idatt2003.mappevurderingprog2.models;

/**
 * The JuliaTransform class implements the Transform2D interface and
 * represents a Julia set transformation.
 * The transformation involves substituting the complex number with the square root of the
 * difference between the complex number and a given point.
 */
public class JuliaTransform implements Transform2D {

  // The point used in the transformation
  private final Complex point;

  // The sign used in the transformation
  private final int sign;

  /**
   * Constructs a JuliaTransform object with the given point and sign.
   *
   * @param point the point to be used in the transformation
   * @param sign  the sign to be used in the transformation
   */
  public JuliaTransform(Complex point, int sign) {
    if (point == null) {
      throw new IllegalArgumentException("Point cannot be null");
    }
    this.point = point;
    this.sign = sign;
  }

  /**
   * Gets the point used in the transformation.
   *
   * @return the point used in the transformation
   */
  public Complex getPoint() {
    return point;
  }

  /**
   * Gets the sign used in the transformation.
   *
   * @return the sign used in the transformation
   */
  public int getSign() {
    return sign;
  }

  /**
   * Transforms the given 2D vector by substituting the complex number with the square root of the
   * difference between the complex number and the given point.
   *
   * @param point the 2D vector to be transformed
   * @return the transformed 2D vector
   */
  @Override
  public Vector2D transform(Vector2D point) {
    if (point == null) {
      throw new IllegalArgumentException("Point cannot be null");
    }
    if (!(point instanceof Complex)) {
      throw new IllegalArgumentException("Point must be an instance of Complex");
    }

    Complex complexPoint = (Complex) point;
    Complex subtracted = complexPoint.subtract(this.point);
    subtracted = subtracted.sqrt();
    if (sign == -1) {
      subtracted = new Complex(-subtracted.getX0(), -subtracted.getX1());
    }
    return subtracted;
  }
}

