package edu.ntnu.idatt2003.mappevurderingprog2.models;

/**
 * This complex class extends the Vector2D and represents a complex number in the form of (a + bi)
 * where "a" is the real part and "b" is the imaginary part.
 * The class also contains a method to calculate the square root of the complex number.
 */
public class Complex extends Vector2D {

  /**
   * Constructor for the complex number.
   *
   * @param realPart      the real part of the complex number
   * @param imaginaryPart the imaginary part of the complex number
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Method to get the real part of the complex number.
   *
   * @return the real part of the complex number.
   */
  public double getRealPart() {
    return getX0();
  }

  /**
   * Method to get the imaginary part of the complex number.
   *
   * @return the imaginary part of the complex number.
   */
  public double getImaginaryPart() {
    return getX1();
  }

  /**
   * Method to calculate the magnitude of the complex number.
   *
   * @return the magnitude of the complex number.
   */
  @Override
  public Complex add(Vector2D other) {
    if (!(other instanceof Complex)) {
      throw new IllegalArgumentException("The vector must be an instance of Complex");
    }
    return new Complex(getX0() + other.getX0(), getX1() + other.getX1());
  }

  /**
   * Method to calculate the magnitude of the complex number.
   *
   * @return the magnitude of the complex number
   */
  @Override
  public Complex subtract(Vector2D other) {
    if (!(other instanceof Complex)) {
      throw new IllegalArgumentException("The vector must be an instance of Complex");
    }
    return new Complex(getX0() - other.getX0(), getX1() - other.getX1());
  }

  /**
   * Method to calculate the square root of the complex number.
   *
   * @return the square root of the complex number
   */
  public Complex sqrt() {
    double r = Math.sqrt(Math.pow(getX0(), 2) + Math.pow(getX1(), 2));
    double theta = Math.atan2(getX1(), getX0()) / 2.0;
    double sqrtMag = Math.sqrt(r);
    double realPart = sqrtMag * Math.cos(theta);
    double imaginaryPart = sqrtMag * Math.sin(theta);
    return new Complex(realPart, imaginaryPart);
  }
}