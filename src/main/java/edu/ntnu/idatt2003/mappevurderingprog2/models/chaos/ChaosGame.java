package edu.ntnu.idatt2003.mappevurderingprog2.models.chaos;

import edu.ntnu.idatt2003.mappevurderingprog2.models.Complex;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Transform2D;
import edu.ntnu.idatt2003.mappevurderingprog2.models.Vector2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.util.Pair;

/**
 * The ChaosGame class represents a chaos game.
 * The class simulates a chaos game with a given description on a canvas.
 */
public class ChaosGame {

  /** The instance of the ChaosGame (singleton pattern). */
  private static ChaosGame instance;

  /** The canvas of the ChaosGame. */
  private ChaosCanvas canvas;

  /** The description of the ChaosGame. */
  private ChaosGameDescription description;

  /** The number of steps of the ChaosGame. */
  private int steps;

  /** The current point of the ChaosGame. */
  private Vector2D currentPoint;

  /** The random object of the ChaosGame. */
  private Random random;

  /** The number of points that are out of bounds. */
  private int outOfBoundsCount = 0;

  /** The list of observers of the ChaosGame. */
  private List<ChaosGameObserver> observers;

  /** The height of the canvas. */
  private final int height = 200;

  /** The width of the canvas. */
  private final int width = 200;

  /**
   * Constructs a new ChaosGame instance.
   * Initializes the current point to (0, 0), random object, and observers list.
   */
  private ChaosGame() {
    this.currentPoint = new Complex(0, 0);
    this.random = new Random();
    this.observers = new ArrayList<>();
  }

  /**
   * Returns the singleton instance of ChaosGame.
   * If the instance does not exist, it creates a new one.
   *
   * @return the singleton instance of ChaosGame.
   */
  public static synchronized ChaosGame getInstance() {
    if (instance == null) {
      instance = new ChaosGame();
    }
    return instance;
  }

  /**
   * Sets the description of the ChaosGame and resets the game.
   *
   * @param description the description of the ChaosGame.
   */
  public void setDescription(ChaosGameDescription description) {
    resetGame();
    this.description = description;
  }

  /**
   * Sets up the canvas for the ChaosGame using the description's min and max coordinates.
   *
   * @throws IllegalStateException if the description is not set.
   */
  public void setCanvas() {
    if (description == null) {
      throw new IllegalStateException("ChaosGame description is not set.");
    }
    this.canvas = new ChaosCanvas(
        width, height, description.getMinCoords(), description.getMaxCoords());
  }

  /**
   * Sets the number of steps for the ChaosGame.
   *
   * @param steps the number of steps, must be between 0 and 1000000.
   * @throws IllegalArgumentException if the number of steps is out of bounds.
   */
  public void setSteps(int steps) {
    if (steps < 0 || steps > 1000000) {
      throw new IllegalArgumentException("Number of steps must be between 0 and 1000000.");
    }
    this.steps = steps;
  }

  public void resetOutOfBoundsCount() {
    this.outOfBoundsCount = 0;
  }

  /**
   * Resets the ChaosGame by clearing the canvas and resetting the current point.
   */
  private void resetGame() {
    if (this.canvas != null) {
      this.canvas.clear();
    }
    this.currentPoint = new Complex(0, 0);
  }

  /**
   * Adds an observer to the ChaosGame.
   *
   * @param observer the observer to be added.
   * @throws IllegalArgumentException if the observer is null.
   */
  public void addObserver(ChaosGameObserver observer) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null.");
    }
    observers.add(observer);
  }

  /**
   * Removes an observer from the ChaosGame.
   *
   * @param observer the observer to be removed.
   * @throws IllegalArgumentException if the observer is null.
   */
  public void removeObserver(ChaosGameObserver observer) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null.");
    }
    observers.remove(observer);
  }

  /**
   * Notifies all observers that the ChaosGame has been updated.
   */
  protected void notifyChaosGameUpdated() {
    for (ChaosGameObserver observer : observers) {
      observer.onChaosGameUpdated();
    }
  }

  /**
   * Returns the canvas of the ChaosGame.
   *
   * @return the canvas of the ChaosGame.
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * Returns the description of the ChaosGame.
   *
   * @return the description of the ChaosGame.
   */
  public ChaosGameDescription getDescription() {
    return description;
  }

  /**
   * Returns the number of steps of the ChaosGame.
   *
   * @return the number of steps.
   */
  public int getSteps() {
    return steps;
  }

  /**
   * Returns the number of points that are out of bounds.
   *
   * @return the number of points that are out of bounds.
   */
  public int getOutOfBoundsCount() {
    return outOfBoundsCount;
  }

  /**
   * Returns the list of observers of the ChaosGame.
   *
   * @return the list of observers.
   */
  public List<ChaosGameObserver> getObservers() {
    return observers;
  }

  /**
   * Runs the chaos game for the set number of steps.
   * Updates the current point based on weighted or non-weighted transformations.
   * Notifies observers after completing the steps.
   */
  public void runSteps() {
    ChaosGameDescription description = ChaosGame.getInstance().getDescription();
    outOfBoundsCount = 0;

    if (description.isWeighted()) {
      List<Pair<Transform2D, Double>> weightedTransforms = description.getWeightedTransforms();
      for (int i = 0; i < steps; i++) {
        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (Pair<Transform2D, Double> weightedTransform : weightedTransforms) {
          cumulativeProbability += weightedTransform.getValue();
          if (p <= cumulativeProbability) {
            currentPoint = weightedTransform.getKey().transform(currentPoint);
            break;
          }
        }
        if (!canvas.putPixel(currentPoint)) {
          outOfBoundsCount++;
        }
      }
    } else {
      List<Transform2D> transforms = description.getTransforms();
      for (int i = 0; i < steps; i++) {
        int randomIndex = random.nextInt(transforms.size());
        Transform2D transform = transforms.get(randomIndex);
        currentPoint = transform.transform(currentPoint);
        if (!canvas.putPixel(currentPoint)) {
          outOfBoundsCount++;
        }
      }
    }
    notifyChaosGameUpdated();
  }
}