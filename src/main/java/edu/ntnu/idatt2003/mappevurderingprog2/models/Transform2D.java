package edu.ntnu.idatt2003.mappevurderingprog2.models;

/**
 * This class is the interface for 2D transformations.
 */
public interface Transform2D {
  public Vector2D transform(Vector2D point);
}

