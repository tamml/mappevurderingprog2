package edu.ntnu.idatt2003.mappevurderingprog2.models;

/**
 * This class represents a 2D affine transformation, and implements the Transform2D interface.
 * It applies a linear transformation to a 2D vector, and then adds a translation vector.
 */
public class AffineTransform2D implements Transform2D {
  // The linear transformation matrix
  private Matrix2x2 matrix;

  // The translation vector
  private Vector2D vector;

  /**
   * Constructs an AffineTransform2D object with the given matrix and vector.
   *
   * @param matrix the linear transformation matrix
   * @param vector the translation vector
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    if (matrix == null) {
      throw new IllegalArgumentException("Matrix cannot be null");
    }
    if (vector == null) {
      throw new IllegalArgumentException("Vector cannot be null");
    }
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Returns the linear transformation matrix.
   *
   * @return the linear transformation matrix
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the translation vector.
   *
   * @return the translation vector
   */
  public Vector2D getVector() {
    return vector;
  }

  /**
   * Transforms the given 2D vector by applying the linear transformation
   * and adding the translation vector.
   *
   * @param point the 2D vector to be transformed
   * @return the transformed 2D vector
   */
  @Override
  public Vector2D transform(Vector2D point) {
    if (point == null) {
      throw new IllegalArgumentException("Point cannot be null");
    }
    return matrix.multiply(point).add(vector);
  }
}