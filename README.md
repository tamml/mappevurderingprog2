# IDATT2003 Mappevurdering
***
# ChaosGame

## Description
This project is a JavaFX application designed for creating a chaos game.
Users can add points to a canvas and connect them with lines to set up the game. 
They can then select a starting point and specify the number of iterations for the chaos game. 
During each iteration, a point will be drawn on the canvas at a random location between 
the starting point and one of the user-added points. 
Additionally, users can save and load chaos games from files.


***

## Installation Manual
**Prerequisites:**
- **Java Development Kit (JDK) 21 or later**
- **Maven**
- **Git**
- **IntelliJ IDEA (Optional if you want to run it with the run button in IntelliJ IDEA)**

**N.B. Make sure that you download it for the correct operating systems (Windows, Mac, Linux etc...)**

**Link to download JDK 21:**
https://www.oracle.com/java/technologies/downloads/#java11

**Link to download Maven:**
https://maven.apache.org/download.cgi


**Link to download IntelliJ IDEA (Optional):**
https://www.jetbrains.com/idea/download/?section=windows

**Link to download Javafx (Optional, to use the run button in intelliJ):**
https://openjfx.io/


## How to clone the project:
1. **Open the terminal to the directory where you want to clone the repository. Then run the following command:**

<pre>git clone https://gitlab.stud.idi.ntnu.no/tamml/mappevurderingprog2.git </pre> 


2. **Switch to the correct branch**

The default branch should be master, if not you can write this in the terminal:
<pre>git checkout master</pre>


3. **Build the project with Maven**

Navigate to the root directory of the project (the directory containing the pom.xml file) and run the following command:

<pre>mvn clean install </pre>

This command cleans any previous builds, compiles the code, runs any tests, and installs the packaged code to your local Maven repository.


## How to run the project:

**With maven:**

Make sure that you have cloned the project if not follow the instructions above under the (How to clone the project).

To run your project with Maven, you can use the mvn javafx:run command. This command is part of the JavaFX Maven plugin, which should be included in your pom.xml file. Open a terminal, navigate to your project directory (the one containing the pom.xml file), and run the command:
<pre>mvn javafx:run</pre>

---

**With the run button in IntelliJ IDEA:**

Make sure that you have cloned the project if not follow the instructions above under the (How to clone the project).

To run the program with the run button in IntelliJ you need to make sure that you have all the prerequisites downloaded.

**Open your project in IntelliJ IDEA.**

1. **Go to Run -> Edit Configurations... in the menu.**
2. **Click on the + button to add a new configuration.**
3. **Select Application.**
4. **In the Main class field, enter:**
<pre>edu.ntnu.idatt2003.mappevurderingprog2.MyApp</pre>  
5. **In the Use classpath of module field, select your project.**
6. **Click OK to save the configuration.**


Now, you can run your project by clicking the green run button in the toolbar or by pressing Shift + F10.  Please note that you need to have the JavaFX SDK installed on your machine to run the JavaFX application.